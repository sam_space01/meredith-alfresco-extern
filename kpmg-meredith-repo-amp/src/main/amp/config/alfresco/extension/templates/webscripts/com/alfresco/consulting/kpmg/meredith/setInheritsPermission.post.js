var json = jsonUtils.toObject(requestbody.content);
var referenceType = "node"; 
var reference = ["workspace", "SpacesStore", json.nodeRef];
var node = search.findNode(referenceType, reference);

var inherit = json.inherit.toString() == "true" ? true : json.inherit.toString() == "false" ? false : null;
 
 if (node != null && inherit != null && json.directPermissionGroups !== null) {
	var directPermissionGroups = json.directPermissionGroups;
	model.directPermissionGroups = [];
 	node.setInheritsPermissions(inherit);
	
	for (i = 0; i < directPermissionGroups.length; i++) {
		if (directPermissionGroups[i] != "") {
			if (inherit == true)
				node.removePermission("Coordinator", directPermissionGroups[i]);
			else
				node.setPermission("Coordinator", directPermissionGroups[i]);
			model.directPermissionGroups.push(directPermissionGroups[i]);
		}
	}
	
	node.save();
	model.status = "200";
	model.directPermissionGroups = JSON.stringify(model.directPermissionGroups);
 }
 else {
	model.status = "400";
	model.directPermissionGroups = [];
 }
 