package com.alfresco.consulting.kpmg.meredith.exception;

public class MeredithException extends Exception {

	private static final long serialVersionUID = -5701063838321418257L;
	
	public MeredithException(String message){
		super(message);
	}
	
	public MeredithException(String message, Throwable cause){
		super(message, cause);
	}
}
