package com.alfresco.consulting.kpmg.meredith.webscript.model;

public class DataRoomName {
    private final String value;

    public DataRoomName(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
