package com.alfresco.consulting.kpmg.meredith.action;

import java.util.List;

import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.log4j.Logger;

import com.alfresco.consulting.kpmg.meredith.service.index.MeredithIndexService;

public class MeredithIndexActionExecuter extends ActionExecuterAbstractBase {

	//Alfresco Action Name
	public static final String NAME = "meredith-index";
	
	//Alfresco Action Parameter Name
	public static final String PARAM_RECURSION = "meredith-index-recursion";

	//Logger
    private Logger logger = Logger.getLogger(MeredithIndexActionExecuter.class);
	
    //Dependencies
	private MeredithIndexService meredithIndexService;
	
	public MeredithIndexService getMeredithIndexService() {
		return meredithIndexService;
	}
	public void setMeredithIndexService(MeredithIndexService meredithIndexService) {
		this.meredithIndexService = meredithIndexService;
	}

    /**
     * @see org.alfresco.repo.action.executer.ActionExecuter#execute(org.alfresco.repo.ref.NodeRef, org.alfresco.repo.ref.NodeRef)
     */
    public void executeImpl(Action ruleAction, NodeRef actionedUponNodeRef) {
    	
    	//Index the Meredith Node (Content or Folder)
    	final Boolean recursion = (Boolean) ruleAction.getParameterValue(PARAM_RECURSION);
    	logger.info("Indexing Meredith Node: " + actionedUponNodeRef + " - Recursion: " + recursion);
    	meredithIndexService.indexNode(actionedUponNodeRef, (Boolean) ruleAction.getParameterValue(PARAM_RECURSION));
    }

	@Override
	protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
		
		//Determines whether the Action executes against the actioned node only or the entire node hierarchy.
		paramList.add(
		        new ParameterDefinitionImpl(
		        		PARAM_RECURSION, DataTypeDefinition.BOOLEAN, true, getParamDisplayLabel(PARAM_RECURSION)));
	}
}