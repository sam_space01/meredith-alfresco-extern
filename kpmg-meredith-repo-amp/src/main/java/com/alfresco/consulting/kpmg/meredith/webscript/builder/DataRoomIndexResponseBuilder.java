package com.alfresco.consulting.kpmg.meredith.webscript.builder;

import java.util.HashMap;
import java.util.Map;

public class DataRoomIndexResponseBuilder {
	
	//WebScript Request Paramaters
    private static final String RESPONSE_MESSAGE = "message";

    public static Map<String, Object> build(final String theDataRoomName) {
		
    	//Construct the Response
		final Map<String, Object> theModel = new HashMap<String, Object>();
		theModel.put(RESPONSE_MESSAGE, "Indexing Meredith Data Room: " + theDataRoomName);
		return theModel;
    }
}
