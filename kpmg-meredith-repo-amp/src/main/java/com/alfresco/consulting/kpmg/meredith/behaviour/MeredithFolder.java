package com.alfresco.consulting.kpmg.meredith.behaviour;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.Behaviour.NotificationFrequency;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;

import com.alfresco.consulting.kpmg.meredith.action.MeredithIndexActionExecuter;
import com.alfresco.consulting.kpmg.meredith.model.MeredithModel;

public class MeredithFolder implements NodeServicePolicies.OnCreateNodePolicy, NodeServicePolicies.OnSetNodeTypePolicy {
	
    //Logger
    private Logger logger = Logger.getLogger(MeredithFolder.class);
    
    //Behaviours
    private Behaviour onCreateNode;
    private Behaviour onSetNodeType;

    //Policies
	private PolicyComponent policyComponent;

	//Dependencies
    private ServiceRegistry serviceRegistry;
    
    //Alfresco Services
    private ActionService actionService;
    private NodeService nodeService;
    
    //Spring Getters / Setters
    public ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}
	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}
	public PolicyComponent getPolicyComponent() {
		return policyComponent;
	}
	public void setPolicyComponent(PolicyComponent policyComponent) {
		this.policyComponent = policyComponent;
	}

    /**
     * Initialise the Meredith Content Behaviour
     */
    public void init() {
    	
    	//Obtain the Alfresco Services
    	actionService = serviceRegistry.getActionService();
    	nodeService = serviceRegistry.getNodeService();
    	
        //Create the Meredith Content Behaviour
    	logger.info("Creating Meredith Content Behaviour ...");
        this.onCreateNode = new JavaBehaviour(this, "onCreateNode", NotificationFrequency.TRANSACTION_COMMIT);
        this.onSetNodeType = new JavaBehaviour(this, "onSetNodeType", NotificationFrequency.TRANSACTION_COMMIT);

        //Bind Behaviours
        logger.info("Binding Policies to Meredith Folder Behaviour ...");
        this.policyComponent.bindClassBehaviour(QName.createQName(NamespaceService.ALFRESCO_URI, "onCreateNode"), MeredithModel.TYPE_MRD_FOLDER, this.onCreateNode);
        this.policyComponent.bindClassBehaviour(QName.createQName(NamespaceService.ALFRESCO_URI, "onSetNodeType"), ContentModel.TYPE_FOLDER, this.onSetNodeType);
    }
    
    @Override
	public void onCreateNode(ChildAssociationRef childAssocRef) {
    	
    	//Add Index if creating Meredith Content
    	logger.debug("Meredith Folder - On Create Node: " + childAssocRef.getChildRef());
    	indexNode(childAssocRef.getChildRef());
	}
    
	@Override
	public void onSetNodeType(NodeRef nodeRef, QName oldType, QName newType) {
		
		//Add Index if specialised to Meredith Folder
		if(newType.equals(MeredithModel.TYPE_MRD_FOLDER)){
			logger.debug("Meredith Folder - On Set Node Type: " + nodeRef);
			indexNode(nodeRef);	
		}
	}
	
	/**
	 * Indexes the Meredith Node
	 * @param nodeRef
	 * 			the NodeRef
	 */
	private void indexNode(final NodeRef nodeRef){

		//Index the Meredith Folder if it is not a Client or Data Room Folder
		if(isMeredithIndexableFolder(nodeRef)){
			
			//Create the Action Parameters - Recursion Not Applicable for Content
			final Map<String, Serializable> theActionParameters = new HashMap<String, Serializable>();
			theActionParameters.put(MeredithIndexActionExecuter.PARAM_RECURSION, false);

			//Create the Action - Asynchronous Not Applicable for Content
			final Action theIndexAction = actionService.createAction(MeredithIndexActionExecuter.NAME, theActionParameters);
			actionService.executeAction(theIndexAction, nodeRef, false, false);
		}
	}
	
	/**
	 * Determines whether the Meredith Folder should be indexed.
	 * @param 	nodeRef
	 * 				the Meredith Folder
	 * @return	boolean
	 * 				whether the folder should be indexed.
	 */
	private boolean isMeredithIndexableFolder(final NodeRef nodeRef) {
		
		//Folder is a Meredith Client or Meredith Data Room
		if (nodeService.hasAspect(nodeRef, MeredithModel.ASPECT_MRD_CLIENT) ||
			nodeService.hasAspect(nodeRef, MeredithModel.ASPECT_MRD_DATA_ROOM) ||
			nodeService.hasAspect(nodeService.getPrimaryParent(nodeRef).getParentRef(), MeredithModel.ASPECT_MRD_DATA_ROOM)) {
			return false;
		} else {
			logger.debug("Folder is indexable: " + nodeRef.getId());
			return true;
		}
	}
}