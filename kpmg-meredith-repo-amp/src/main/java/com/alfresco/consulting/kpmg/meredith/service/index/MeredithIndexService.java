package com.alfresco.consulting.kpmg.meredith.service.index;

import org.alfresco.service.cmr.repository.NodeRef;

public interface MeredithIndexService {
	
	/**
	 * Indexes the Meredith Node
	 * @param 	theNodeRef
	 * 				the NodeRef to be indexed.
	 * @param 	recursion
	 * 				whether the indexed is performed recursively down the node hierarchy.
	 */
	public void indexNode(NodeRef theNodeRef, boolean recursion);
}
