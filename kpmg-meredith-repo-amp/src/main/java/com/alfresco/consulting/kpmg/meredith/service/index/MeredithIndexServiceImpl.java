package com.alfresco.consulting.kpmg.meredith.service.index;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.lock.LockService;
import org.alfresco.service.cmr.lock.LockStatus;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;

import com.alfresco.consulting.kpmg.meredith.model.MeredithModel;

public class MeredithIndexServiceImpl implements MeredithIndexService {
	
	private static final String INDEX_SEPARATOR = ".";

	//Logger
    private Logger logger = Logger.getLogger(MeredithIndexServiceImpl.class);
    
    //Dependencies
	private ServiceRegistry serviceRegistry;
	
	//Alfresco Services
	private NodeService nodeService;
	private LockService lockService;
	private FileFolderService fileFolderService;
		
	public ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}
	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}

	@Override
	public void indexNode(final NodeRef theNodeRef, final boolean recursion) {
    	
		//Index the Meredith Node (Content or Folder)
		logger.debug("Applying Meredith Index to NodeRef: " + theNodeRef.getId());
		
		//Obtain the Alfresco Services
		nodeService = serviceRegistry.getNodeService();
		fileFolderService = serviceRegistry.getFileFolderService();
		lockService = serviceRegistry.getLockService();
		
		//Determine the Node Type
		if(nodeService.getType(theNodeRef).equals(MeredithModel.TYPE_MRD_CONTENT)){
			
			//Index a Meredith Content Node
			indexMeredithContent(theNodeRef);
			
		} else if (nodeService.getType(theNodeRef).equals(MeredithModel.TYPE_MRD_FOLDER)){
			
			//Index a Meredith Folder Node
			indexMeredithFolder(theNodeRef, recursion);
		
		} else {
			
			//Log a Warning - Only Meredith Content and Meredith Folders can be indexed
			logger.warn("Unable to index: " + theNodeRef.getId() + " non-Meredith type");
			//TODO - Should throw an Exception
		}
	}
	
	/**
	 * Obtains the Parent Folder for a NodeRef
	 * @param 	theNodeRef
	 * 				the NodeRef of the Child
	 * @return	NodeRef
	 * 				the Parent Folder of the Node
	 */
	private NodeRef getParentFolder(final NodeRef theNodeRef) {
				
		//Obtain all Possible Parents
		final List<ChildAssociationRef> theParentFolders = nodeService.getParentAssocs(theNodeRef);
		
		//Extract the Parent
		NodeRef theParent = null;
		if (theParentFolders.size() == 1){
			
			//Single Parent Folder (Expected)
			theParent = theParentFolders.get(0).getParentRef();
			logger.debug("Identified the parent folder for: " + theNodeRef + " as: " + theParent);
		
		} else {
			
			//Multiple Parent Folders (Should Not Happen)
			//TODO - Throw Exception ???
			logger.warn("Multiple parent folders have been identified for: " + theNodeRef);

		}
		
		//Return the Parent Folder
		return theParent;
	}
	
	/**
	 * Obtains the Index Sequence for a Folder
	 * @param 	theFolder
	 * 				the NodeRef of the Folder
	 * @return	NodeRef
	 * 				the Index Sequence of the Folder
	 */
	private NodeRef getFolderIndexSequence(final NodeRef theFolder) {
				
		//Obtain all Possible Indexes
		final Set<QName> theTypes = new HashSet<QName>();
		theTypes.add(MeredithModel.TYPE_MRD_SEQUENCE);
		final List<ChildAssociationRef> theIndexes = nodeService.getChildAssocs(theFolder, theTypes);
		
		//Extract the Index Sequence
		NodeRef theIndexSequence = null;
		if (theIndexes.size() == 1){
			
			//Single Index Folder
			theIndexSequence = theIndexes.get(0).getChildRef();
			logger.debug("Obtaining Index Sequence: " + theFolder);
		
		} else if (theIndexes.size() == 0){
			
			//Missing Index Sequence
			logger.warn("Index Sequence not found for: " + theFolder);
		
		}
		
		//Return the Index Sequence
		return theIndexSequence;
	}
	
	/**
	 * Indexes a Meredith Document
	 * @param 	theNodeRef
	 * 				theNodeRef of the Document.
	 */
	private void indexMeredithContent(NodeRef theNodeRef) {
		
		//Log the Method Invocation
		logger.debug("Index Meredith Content: " + theNodeRef);
		
		final LockStatus theLockStatus = lockService.getLockStatus(theNodeRef);
		if (theLockStatus.equals(LockStatus.NO_LOCK)) {
			
			//Obtain the Parent Folder of the Folder
			final NodeRef theParentFolder = getParentFolder(theNodeRef);
			
			//Obtain the Index Prefix from the Parent Folder
			final String theIndexPrefix = getIndexPrefix(theParentFolder);
			
			//Obtain the Index Sequence
			final NodeRef theIndexSequence = getFolderIndexSequence(theParentFolder);
			
			//Verify the Index Sequence
			if (theIndexSequence != null) {
				
				//Obtain the Next Sequence Number
				final long theIndexSequenceNumber = (Long) nodeService.getProperty(theIndexSequence, MeredithModel.PROP_MRD_SEQUENCE_NUMBER) + 1;
				nodeService.setProperty(theIndexSequence, MeredithModel.PROP_MRD_SEQUENCE_NUMBER, theIndexSequenceNumber);
				
				//Build the Index Number
				final StringBuilder theBuilder = new StringBuilder();
				if (theIndexPrefix != null){
					theBuilder.append(theIndexPrefix);
					theBuilder.append(INDEX_SEPARATOR);
				}
				theBuilder.append(theIndexSequenceNumber);
				
				//Index the Content
				final Map<QName, Serializable> theProperties = new HashMap<QName, Serializable>();
				theProperties.put(MeredithModel.PROP_MRD_INDEX, theBuilder.toString());
				nodeService.addAspect(theNodeRef, MeredithModel.ASPECT_MRD_DATA_ROOM_INDEX, theProperties);
			}
		} else {
			logger.warn("Node is locked, unable to update! - " + theNodeRef);
		}
		

	}

	/**
	 * Indexes a Meredith Folder
	 * @param 	theNodeRef
	 * 				theNodeRef of the Folder.
	 */
	private void indexMeredithFolder(NodeRef theNodeRef, boolean recursion) {
		
		//Log the Method Invocation
		logger.debug("Indexing Meredith Folder: " + theNodeRef);
		
		//Obtain the Parent Folder of the Folder
		final NodeRef theParentFolder = getParentFolder(theNodeRef);
		
		//Obtain the Index Prefix from the Parent Folder
		final String theIndexPrefix = getIndexPrefix(theParentFolder);

		//Obtain the Index Sequences
		NodeRef theParentFolderIndexSequence = getFolderIndexSequence(theParentFolder);
		logger.debug("Index: " + theParentFolderIndexSequence);
		NodeRef theIndexSequence = getFolderIndexSequence(theNodeRef);
		
		
		//Verify the Index Sequence
		if (theIndexSequence != null) {
			
			//Reset the Index
			nodeService.setProperty(theIndexSequence, MeredithModel.PROP_MRD_SEQUENCE_NUMBER, 0);
			logger.debug("Index: " + theIndexSequence);
			
		} else {
			
			//Create the Index
			logger.debug("Creating Index");
			theIndexSequence = nodeService.createNode(theNodeRef, ContentModel.ASSOC_CONTAINS, MeredithModel.TYPE_MRD_SEQUENCE, MeredithModel.TYPE_MRD_SEQUENCE).getChildRef();
			nodeService.setProperty(theIndexSequence, MeredithModel.PROP_MRD_SEQUENCE_NUMBER, 0);
			logger.debug("Index: " + theIndexSequence);
		}
		
		//Verify the Parent Index Sequence
		if (theParentFolderIndexSequence != null) {
			
			logger.debug("Parent Index: " + theParentFolderIndexSequence);
			//Obtain the Next Sequence Number
			
			final Object theSeqNum = nodeService.getProperty(theParentFolderIndexSequence, MeredithModel.PROP_MRD_SEQUENCE_NUMBER);
			if (theSeqNum == null){
				logger.debug("Null Sequence Number!");
			}
			
			final long theIndexSequenceNumber = (Long) nodeService.getProperty(theParentFolderIndexSequence, MeredithModel.PROP_MRD_SEQUENCE_NUMBER) + 1;
			nodeService.setProperty(theParentFolderIndexSequence, MeredithModel.PROP_MRD_SEQUENCE_NUMBER, theIndexSequenceNumber);
			
			//Build the Index Number
			final StringBuilder theBuilder = new StringBuilder();
			if (theIndexPrefix != null){
				theBuilder.append(theIndexPrefix);
				theBuilder.append(INDEX_SEPARATOR);
			}
			theBuilder.append(theIndexSequenceNumber);
			
			//Index the Content
			final Map<QName, Serializable> theProperties = new HashMap<QName, Serializable>();
			theProperties.put(MeredithModel.PROP_MRD_INDEX, theBuilder.toString());
			nodeService.addAspect(theNodeRef, MeredithModel.ASPECT_MRD_DATA_ROOM_INDEX, theProperties);
		}
		
		//Process Other Nodes if required
		if (recursion) {
			
			//Obtain the Child Folders
			List<FileInfo> theFolders = fileFolderService.listFolders(theNodeRef);
			for (FileInfo theFolder : theFolders){
				indexNode(theFolder.getNodeRef(), recursion);
			}
			
			//Obtain the Child Documents
			List<FileInfo> theDocuments = fileFolderService.listFiles(theNodeRef);
			for (FileInfo theDocument : theDocuments){
				indexNode(theDocument.getNodeRef(), recursion);
			}
		}
	}
	
	/**
	 * Obtains the Index Prefix from a Parent Folder
	 * @param 	theParentFolder
	 * 				the Parent Folder of the Node being indexed.
	 * @return	String
	 * 				the Index Prefix
	 */
	private String getIndexPrefix(final NodeRef theParentFolder) {
		
		//Log the Method Invocation
		logger.debug("Obtaining Index Prefix from: " + theParentFolder);
		
		//Obtain the Index Prefix
		final String theIndexPrefix;
		if(nodeService.hasAspect(theParentFolder, MeredithModel.ASPECT_MRD_DATA_ROOM)){
			
			//The Parent Folder of a Data Room does not have an index
			theIndexPrefix = null;
			logger.debug("Parent Folder is a Data Room; no Index Prefix available");
			
		} else {
			
			//Obtain the Parent Folder Index Prefix
			theIndexPrefix = (String) nodeService.getProperty(theParentFolder, MeredithModel.PROP_MRD_INDEX);
			logger.debug("Index Prefix: " + theIndexPrefix);
		}
		
		//Return the Index Prefix
		return theIndexPrefix;
	}
}
