package com.alfresco.consulting.kpmg.meredith.model;

import org.alfresco.service.namespace.QName;

/**
 * Meredith Model Constants
 */
public interface MeredithModel
{
    /*
     * Meredith Model URI
     */
    static final String MEREDITH_MODEL_1_0_URI = "http://www.kpmg.com/alfresco/meredith/1.0";

    /* 
     * Meredith Model Prefix
     */
    static final String MEREDITH_MODEL_PREFIX = "mrd";
	
    /*
     *  Meredith Type - Definitions
     */
    static final QName TYPE_MRD_SEQUENCE = QName.createQName(MEREDITH_MODEL_1_0_URI, "sequence");
    static final QName PROP_MRD_SEQUENCE_NUMBER = QName.createQName(MEREDITH_MODEL_1_0_URI, "sequenceNumber");
    
    static final QName TYPE_MRD_CONTENT = QName.createQName(MEREDITH_MODEL_1_0_URI, "content");
    
    static final QName TYPE_MRD_FOLDER = QName.createQName(MEREDITH_MODEL_1_0_URI, "folder");
    
    /*
     *  Meredith Aspect - Definitions
     */
    static final QName ASPECT_MRD_CLIENT = QName.createQName(MEREDITH_MODEL_1_0_URI, "client");
    static final QName PROP_MRD_CLIENT_NAME = QName.createQName(MEREDITH_MODEL_1_0_URI, "clientName");
    
    static final QName ASPECT_MRD_DATA_ROOM = QName.createQName(MEREDITH_MODEL_1_0_URI, "dataRoom");
    
    static final QName ASPECT_MRD_DATA_ROOM_INDEX = QName.createQName(MEREDITH_MODEL_1_0_URI, "dataRoomIndex");
    static final QName PROP_MRD_INDEX = QName.createQName(MEREDITH_MODEL_1_0_URI, "index");
}