package com.alfresco.consulting.kpmg.meredith.service.dataroom;

import com.alfresco.consulting.kpmg.meredith.exception.MeredithException;

public class MeredithDataRoomException extends MeredithException {
	
	private static final long serialVersionUID = 8996581270242581591L;

	public MeredithDataRoomException(String message){
		super(message);
	}
	
	public MeredithDataRoomException(String message, Throwable cause){
		super(message, cause);
	}
}
