package com.alfresco.consulting.kpmg.meredith.webscript.model;

public class ClientName {
    private final String value;

    public ClientName(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
