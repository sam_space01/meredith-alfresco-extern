package com.alfresco.consulting.kpmg.meredith.webscript.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Encapsulates the inputs to the Create Data Room webscript.
 */
public class DataRoomCreateRequest {
    private final String clientName;
    private final String dataRoomName;
    private final List<DataRoomAce> seedAcl;

    public DataRoomCreateRequest(String clientName, String dataRoomName, List<DataRoomAce> thePermissions) {
        this.clientName = clientName;
        this.dataRoomName = dataRoomName;
        this.seedAcl = thePermissions;
    }

    public ClientName getClientName() {
        return new ClientName(clientName);
    }

    public DataRoomName getDataRoomName() {
        return new DataRoomName(dataRoomName);
    }

    public List<DataRoomAce> getSeedAcl() {
        return seedAcl;
    }
}
