package com.alfresco.consulting.kpmg.meredith.service.dataroom;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.alfresco.consulting.kpmg.meredith.webscript.builder.DataRoomRenameResponseBuilder;
import com.alfresco.consulting.kpmg.meredith.webscript.model.*;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.evaluator.IsSubTypeEvaluator;
import org.alfresco.repo.action.executer.SpecialiseTypeActionExecuter;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionCondition;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.action.CompositeAction;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.rule.Rule;
import org.alfresco.service.cmr.rule.RuleService;
import org.alfresco.service.cmr.rule.RuleType;
import org.apache.log4j.Logger;
import org.springframework.extensions.webscripts.WebScriptException;

import com.alfresco.consulting.kpmg.meredith.model.MeredithModel;
import com.alfresco.consulting.kpmg.meredith.webscript.builder.DataRoomCreateResponseBuilder;
import com.alfresco.consulting.kpmg.meredith.webscript.model.ClientName;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomAce;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomCreateRequest;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomName;
import com.alfresco.consulting.kpmg.meredith.webscript.repository.FolderRepository;

import org.springframework.http.HttpStatus;

public class MeredithDataRoomServiceImpl implements MeredithDataRoomService {
	
	//Logger
    private Logger logger = Logger.getLogger(MeredithDataRoomServiceImpl.class);
    
    //Dependencies
	private ServiceRegistry serviceRegistry;
	
	public ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}
	
	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}
	
	//Alfresco Services
	private FolderRepository folderRepository;

	public FolderRepository getFolderRepository() {
		return folderRepository;
	}

	public void setFolderRepository(FolderRepository folderRepository){
		this.folderRepository = folderRepository;
	}
	
	@Override
	public Map<String, Object> create(DataRoomCreateRequest theParameters) throws MeredithDataRoomException {
		
		//Validate the Parameters
		if (theParameters.getClientName().getValue() == null || theParameters.getClientName().getValue().equals("")){
			throw new MeredithDataRoomException("Invalid Client Name");
		}
		
		if (theParameters.getDataRoomName().getValue() == null || theParameters.getDataRoomName().getValue().equals("")){
			throw new MeredithDataRoomException("Invalid Data Room Name");
		}
		
		//Log the Method Invocation
		logger.info("Creating Meredith Data Room: " + theParameters.getClientName().getValue() + "->" + theParameters.getDataRoomName().getValue());

		//Obtain the Meredith Data Room Folder
		final NodeRef theDataRooms = findMeredithDataRoomsRoot();
		
		//Create the Client Folder
		final NodeRef theClientFolder = findOrCreateClientFolder(theParameters.getClientName(), theDataRooms);
		
		//Create the Data Room Folder
		final NodeRef theDataRoomFolder = findOrCreateDataRoomFolder(
                theParameters.getDataRoomName(),
                theParameters.getSeedAcl(),
                theClientFolder
        );

		//Create the Documents Folder in the Data Room
		final NodeRef theBidsFolder = findOrCreateDataRoomSubFolder(
				theDataRoomFolder,
				"Bids"
		);

        boolean documentsAlreadyExists = existsIn(theDataRoomFolder, "Documents");

        //Create the Documents Folder in the Data Room
        final NodeRef theDocumentsFolder = findOrCreateDataRoomSubFolder(
                theDataRoomFolder,
				"Documents"
        );

        //Create the Data Room Index
		if(!documentsAlreadyExists) {
            logger.debug("Data Room Index - creating new");
            createDataRoomIndex(theDocumentsFolder);
        } else {
            logger.debug("Data Room Index - using existing");
        }
		
		//Configure the Data Room Folder Rules
		addMeredithContentAction(theDocumentsFolder);
		addMeredithFolderAction(theDocumentsFolder);

        return DataRoomCreateResponseBuilder.build(theClientFolder, theDataRoomFolder, theDocumentsFolder, theBidsFolder);
		
	}
	
	public Map<String, Object> update(DataRoomRenameRequest theParameters) throws MeredithDataRoomException {
		try
		{
			//Obtain the Meredith Data Room Folder
			final NodeRef allDataRoomsFolder = findMeredithDataRoomsRoot();

			// Rename the client folder
			NodeRef clientFolder = findNodeIn(
                    allDataRoomsFolder,
                    theParameters.getClientName().getValue()
            );

            if(clientFolder == null)
                throw new WebScriptException(HttpStatus.NOT_FOUND.value(), "Client folder '" + theParameters.getClientName().getValue() + "' not found in nodref '" + allDataRoomsFolder.getId() + "'");

            NodeRef dataRoomFolder = renameFolderIn(
                    clientFolder,
                    theParameters.getPreviousDataRoomName().getValue(),
                    theParameters.getNewDataRoomName().getValue()
            );

			return DataRoomRenameResponseBuilder.build(clientFolder, dataRoomFolder);
		}
		catch (FileNotFoundException fileNotFoundEx)
		{
			throw new MeredithDataRoomException("Name collision, could not process Data Room update request.");
		}
	}
	
	private NodeRef renameFolderIn(NodeRef parent, String currentName, String newName) throws FileNotFoundException {
        NodeRef folderToRename = findNodeIn(parent, currentName);

        if(folderToRename == null)
            throw new WebScriptException(HttpStatus.NOT_FOUND.value(), "Folder '" + currentName + "' not found in nodref '" + parent.getId() + "'");

        if(existsIn(parent, newName))
            throw new WebScriptException(HttpStatus.CONFLICT.value(), "Folder '" + newName + "' already exists in nodref '" + parent.getId() + "'");

        FileInfo renamedFileInfo = serviceRegistry.getFileFolderService().rename(folderToRename, newName);

        return renamedFileInfo.getNodeRef();
    }

    private NodeRef findNodeIn(NodeRef parent, String name){
        return getServiceRegistry().getFileFolderService().searchSimple(parent, name);
    }

    private boolean existsIn(NodeRef parentFolder, String folderName){
        return (findNodeIn(parentFolder, folderName) != null);
    }
	
	/**
	 * Obtains the Client Folder
	 * @param 	theClientName
	 * 				the Client Name
	 * @param 	theDataRooms
	 * 				the Data Rooms root folder
	 * @return	
	 */
	private NodeRef findOrCreateClientFolder(final ClientName theClientName, final NodeRef theDataRooms) {
		
		//Log the Method Invocation
		logger.debug("Obtaining the Client Folder for: " + theClientName.getValue());
		
		final NodeRef theClientFolder = findNodeIn(theDataRooms, theClientName.getValue());

		//Obtain the Client Folder NodeRef
		if(theClientFolder != null){
			
			return theClientFolder;	

		} else {
			
			//Create the Client folder
			logger.debug("Creating Client Folder: " + theClientName.getValue());
            return getFolderRepository().createClientFolder(theDataRooms, theClientName);
		}
	}
	
	private NodeRef findOrCreateDataRoomFolder(final DataRoomName dataRoomName,
                                               final List<DataRoomAce> list,
                                               final NodeRef theClientFolder) {
		
		final NodeRef theDataRoomFolder = findNodeIn(theClientFolder, dataRoomName.getValue());
		
		//Obtain the Client Folder NodeRef
		if(theDataRoomFolder != null){
			
			return theDataRoomFolder;	

		} else {
			
			//Create the Client folder
			logger.debug("Creating Data Room Name Folder: " + dataRoomName.getValue());
			return getFolderRepository().createDataRoomFolder(theClientFolder, dataRoomName, list);
		}
	}

    private NodeRef findOrCreateDataRoomSubFolder(final NodeRef theDataRoomFolder,
												  final String folderName) {
    	
		final NodeRef theSubFolder = findNodeIn(theDataRoomFolder, folderName);
		
		//Obtain the Sub NodeRef
		if(theSubFolder != null){
			
			return theSubFolder;	

		} else {
			
			//Create the Sub folder
			logger.debug("Creating Sub Folder: " + folderName);
			return getFolderRepository().createDataRoomSubFolder(theDataRoomFolder, folderName);
		}
    }

    /**
	 * Obtains the Meredith Data Rooms Folder
     * @throws MeredithDataRoomException 
	 */
	private NodeRef findMeredithDataRoomsRoot() throws MeredithDataRoomException {
		
        NodeRef theDataRoomHome = getFolderRepository().findDataRoomsRoot();

		//Obtain the Meredith Data Room NodeRef
		if(theDataRoomHome != null){
            return theDataRoomHome;
		} else {
			
			//Meredith Data Rooms Folder must exist (/Meredith/Data Rooms)
			throw new MeredithDataRoomException("Meredith Data Rooms folder could not be obtained");
		}
	}

	/**
	 * Creates the Index Sequence for a Data Room
	* @param 	theDataRoom
	* 				the Data Room in which the index should be created.
	* @return	NodeRef
	* 				theNodeRef of the Index Sequence.
	*/
	private NodeRef createDataRoomIndex(final NodeRef theDataRoom){
	
		//Log the Method Invocation
		logger.debug("Creating Data Room Index for: " + theDataRoom);
	
		NodeService nodeService = serviceRegistry.getNodeService();
		
		//Create the Initial Index Sequence
		final ChildAssociationRef theIndexSequence = nodeService.createNode(
				theDataRoom,
				ContentModel.ASSOC_CONTAINS,
				MeredithModel.TYPE_MRD_SEQUENCE,
				MeredithModel.TYPE_MRD_SEQUENCE);
		nodeService.setProperty(theIndexSequence.getChildRef(), MeredithModel.PROP_MRD_SEQUENCE_NUMBER, 0);
		return theIndexSequence.getChildRef();
	}
	
	/**
	 * Adds a Folder Rule to Specialise Type to Meredith Content.
	 * @param 	theParentFolder
	 * 				the Folder to apply the rule to.
	 */
	private void addMeredithContentAction(final NodeRef theParentFolder) {
		
		//Log the Method Invocation
		logger.debug("Adding Meredith Content Action: " + theParentFolder.getId());

		ActionService theActionService = serviceRegistry.getActionService();
		RuleService theRuleService = serviceRegistry.getRuleService();

		//Create a Folder Rule
		final Rule theFolderRule = new Rule();
		theFolderRule.setRuleType(RuleType.INBOUND);
		theFolderRule.setTitle("Meredith Content Specialisation Rule");
		theFolderRule.applyToChildren(true);
		
		//Create the Rule Action
		final CompositeAction compositeAction = theActionService.createCompositeAction();
		theFolderRule.setAction(compositeAction);
		
		//Create the Action Conditions
		final ActionCondition actionCondition = theActionService.createActionCondition(IsSubTypeEvaluator.NAME); 
		final Map<String, Serializable> conditionParameters = new HashMap<String, Serializable>(1);
		conditionParameters.put(IsSubTypeEvaluator.PARAM_TYPE, ContentModel.TYPE_CONTENT);
		actionCondition.setParameterValues(conditionParameters); 
		compositeAction.addActionCondition(actionCondition);

		//Set the Action
		final Action action = theActionService.createAction(SpecialiseTypeActionExecuter.NAME);            
		action.setTitle("Meredith Content Specialisation");
		action.setExecuteAsynchronously(false);
		
		//Set the Action Parameters
		final Map<String, Serializable> ruleParameters = new HashMap<String, Serializable>(1);
		ruleParameters.put(SpecialiseTypeActionExecuter.PARAM_TYPE_NAME, MeredithModel.TYPE_MRD_CONTENT);	
		action.setParameterValues(ruleParameters);
		compositeAction.addAction(action);
		
		//Save the Folder Rule
		theRuleService.saveRule(theParentFolder, theFolderRule);
	}
	
	/**
	 * Adds a Folder Rule to Specialise Type to Meredith Folder.
	 * @param 	theParentFolder
	 * 				the Folder to apply the rule to.
	 */
	private void addMeredithFolderAction(final NodeRef theParentFolder) {
		
		//Log the Method Invocation
		logger.debug("Adding Meredith Folder Action: " + theParentFolder.getId());

		ActionService theActionService = serviceRegistry.getActionService();
		RuleService theRuleService = serviceRegistry.getRuleService();

		//Create a Folder Rule
		final Rule theFolderRule = new Rule();
		theFolderRule.setRuleType(RuleType.INBOUND);
		theFolderRule.setTitle("Meredith Folder Specialisation Rule");
		theFolderRule.applyToChildren(true);
		
		//Create the Rule Action
		final CompositeAction compositeAction = theActionService.createCompositeAction();
		theFolderRule.setAction(compositeAction);
		
		//Create the Action Conditions
		final ActionCondition actionCondition = theActionService.createActionCondition(IsSubTypeEvaluator.NAME); 
		final Map<String, Serializable> conditionParameters = new HashMap<String, Serializable>(1);
		conditionParameters.put(IsSubTypeEvaluator.PARAM_TYPE, ContentModel.TYPE_FOLDER);
		actionCondition.setParameterValues(conditionParameters); 
		compositeAction.addActionCondition(actionCondition);

		//Set the Action
		final Action action = theActionService.createAction(SpecialiseTypeActionExecuter.NAME);            
		action.setTitle("Meredith Folder Specialisation");
		action.setExecuteAsynchronously(false);
		
		//Set the Action Parameters
		final Map<String, Serializable> ruleParameters = new HashMap<String, Serializable>(1);
		ruleParameters.put(SpecialiseTypeActionExecuter.PARAM_TYPE_NAME, MeredithModel.TYPE_MRD_FOLDER);	
		action.setParameterValues(ruleParameters);
		compositeAction.addAction(action);
		
		//Save the Folder Rule
		theRuleService.saveRule(theParentFolder, theFolderRule);
	}
}
