package com.alfresco.consulting.kpmg.meredith.webscript.repository;

import com.alfresco.consulting.kpmg.meredith.webscript.model.ClientName;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomAce;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomName;

import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.search.SearchService;

import java.util.List;

/**
 * Created by gambs on 23/03/2016.
 */
public interface FolderRepository {

    ServiceRegistry getServiceRegistry();
    void setServiceRegistry(ServiceRegistry serviceRegistry);

    SearchService getSearchService();

    NodeRef findDataRoomsRoot();

    NodeRef createClientFolder(NodeRef dataRoomsRootNodeRef, ClientName clientName);

    NodeRef createDataRoomFolder(NodeRef clientNodeRef, DataRoomName dataRoomName, List<DataRoomAce> thePermissions);

    NodeRef createDataRoomSubFolder(NodeRef dataRoomNodeRef, String name);

}
