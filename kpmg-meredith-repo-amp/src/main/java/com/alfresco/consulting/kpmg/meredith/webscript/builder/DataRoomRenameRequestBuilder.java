package com.alfresco.consulting.kpmg.meredith.webscript.builder;

import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomRenameRequest;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import javax.servlet.http.HttpServletResponse;

public class DataRoomRenameRequestBuilder {
    private static Logger logger = Logger.getLogger(DataRoomRenameRequestBuilder.class);

    //WebScript Request Parameters
    public static final String PARAM_PREV_DATA_ROOM_NAME = "currentDataRoomName";
    public static final String PARAM_NEW_DATA_ROOM_NAME = "newDataRoomName";
    public static final String PARAM_CLIENT_NAME = "clientName";

    /**
     * Obtains the Parameters from the Request
     *
     * Extract the Parameters from the Request
     {
       "clientName":"clientName",
       "currentDataRoomName":"dataRoomName",
       "newDataRoomName":"dataRoomName"
     }
     * @param 	theRequest
     * @return	Map<String,Object>
     * 				the Webscript Parameters
     */
    public static DataRoomRenameRequest build(final WebScriptRequest theRequest) {
        //Log the Method Invocation
        logger.debug("Obtaining Parameters from Request: " + theRequest.getURL());

        //Obtain the JSON from the Request
        final Object theJSONObject = theRequest.parseContent();

        //Validate the JSON
        if (theJSONObject == null || !(theJSONObject instanceof JSONObject)) {
            throw new WebScriptException(HttpServletResponse.SC_BAD_REQUEST, "Invalid Request");
        }

        return build((JSONObject)theJSONObject);
    }

    public static DataRoomRenameRequest build(final JSONObject theJSON) {

        try {
            //Extract the Parameters
            return new DataRoomRenameRequest(
                    extractStringFrom(theJSON, PARAM_CLIENT_NAME),
                    extractStringFrom(theJSON, PARAM_PREV_DATA_ROOM_NAME),
                    extractStringFrom(theJSON, PARAM_NEW_DATA_ROOM_NAME)
            );

        } catch (JSONException e) {
            throw new WebScriptException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        }
    }

    public static String extractStringFrom(final JSONObject theJSON, String theFieldName) throws JSONException {
        final String theValue = theJSON.getString(theFieldName);

        if (theValue == null || theValue.isEmpty()){
            throw new WebScriptException("Parameter: " + theFieldName + " has not been provided");
        }

        return theValue;
    }
}
