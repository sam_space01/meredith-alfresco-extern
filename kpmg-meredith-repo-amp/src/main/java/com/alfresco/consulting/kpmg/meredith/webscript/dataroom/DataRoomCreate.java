package com.alfresco.consulting.kpmg.meredith.webscript.dataroom;

import java.util.Map;

import com.alfresco.consulting.kpmg.meredith.service.dataroom.MeredithDataRoomException;
import com.alfresco.consulting.kpmg.meredith.service.dataroom.MeredithDataRoomService;
import com.alfresco.consulting.kpmg.meredith.webscript.builder.DataRoomCreateRequestBuilder;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomCreateRequest;

import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.http.HttpStatus;

public class DataRoomCreate extends DeclarativeWebScript {
	
    //Dependencies
    private MeredithDataRoomService meredithDataRoomService;
	
	protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache){
		
		//Log the Method Invocation
		try {
			
			//Create the Data Room
			final DataRoomCreateRequest theParameters = DataRoomCreateRequestBuilder.build(req);
			return meredithDataRoomService.create(theParameters);
			
		} catch (MeredithDataRoomException mrde){
			throw new WebScriptException(HttpStatus.INTERNAL_SERVER_ERROR.value(), mrde.getMessage());
		}
	}

	public MeredithDataRoomService getMeredithDataRoomService() {
		return meredithDataRoomService;
	}

	public void setMeredithDataRoomService(MeredithDataRoomService meredithDataRoomService) {
		this.meredithDataRoomService = meredithDataRoomService;
	}
}

