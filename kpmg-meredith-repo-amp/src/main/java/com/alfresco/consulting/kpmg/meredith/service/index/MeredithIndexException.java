package com.alfresco.consulting.kpmg.meredith.service.index;

import com.alfresco.consulting.kpmg.meredith.exception.MeredithException;

public class MeredithIndexException extends MeredithException {
	
	private static final long serialVersionUID = 8037374722867530171L;

	public MeredithIndexException(String message){
		super(message);
	}
	
	public MeredithIndexException(String message, Throwable cause){
		super(message, cause);
	}
}
