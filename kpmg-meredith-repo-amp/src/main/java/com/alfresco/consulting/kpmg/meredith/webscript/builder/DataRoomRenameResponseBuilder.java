package com.alfresco.consulting.kpmg.meredith.webscript.builder;

import org.alfresco.service.cmr.repository.NodeRef;

import java.util.HashMap;
import java.util.Map;

public class DataRoomRenameResponseBuilder {
    private static final String RESPONSE_DATA_ROOM_FOLDER = "dataRoomFolder";
    private static final String RESPONSE_CLIENT_FOLDER = "clientFolder";

    public static Map<String, Object> build(NodeRef theClientFolder,
                                            NodeRef theDataRoomFolder) {
        //Construct the Response
        final Map<String, Object> theModel = new HashMap<String, Object>();
        theModel.put(RESPONSE_CLIENT_FOLDER, theClientFolder.getId());
        theModel.put(RESPONSE_DATA_ROOM_FOLDER, theDataRoomFolder.getId());
        return theModel;
    }
}
