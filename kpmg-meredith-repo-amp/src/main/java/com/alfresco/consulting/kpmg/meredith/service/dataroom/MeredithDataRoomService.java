package com.alfresco.consulting.kpmg.meredith.service.dataroom;

import java.util.Map;

import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomCreateRequest;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomRenameRequest;

public interface MeredithDataRoomService {
	
	/**
	 * Creates a Meredith Data Room
	 * @param 	theRequest
	 */
	Map<String,Object> create(DataRoomCreateRequest theRequest) throws MeredithDataRoomException;

	/**
	 * Updates the name of the folders that make up a Data Room in the Document Repo.
	 * @param theRequest
	 * @throws MeredithDataRoomException 
     */
	Map<String,Object> update(DataRoomRenameRequest theRequest) throws MeredithDataRoomException;
}
