package com.alfresco.consulting.kpmg.meredith.webscript.model;

public class DataRoomRenameRequest {
    private final String clientName;
    private final String currentDataRoomName;
    private final String newDataRoomName;

    public DataRoomRenameRequest(String clientName, String currentDataRoomName, String newDataRoomName) {
        this.clientName = clientName;
        this.currentDataRoomName = currentDataRoomName;
        this.newDataRoomName = newDataRoomName;
    }

    public ClientName getClientName() {
        return new ClientName(clientName);
    }
    public DataRoomName getPreviousDataRoomName() {
        return new DataRoomName(currentDataRoomName);
    }
    public DataRoomName getNewDataRoomName() {
        return new DataRoomName(newDataRoomName);
    }
}
