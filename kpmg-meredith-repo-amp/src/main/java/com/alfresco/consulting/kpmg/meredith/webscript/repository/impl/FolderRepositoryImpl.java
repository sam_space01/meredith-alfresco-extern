package com.alfresco.consulting.kpmg.meredith.webscript.repository.impl;

import com.alfresco.consulting.kpmg.meredith.model.MeredithModel;
import com.alfresco.consulting.kpmg.meredith.webscript.model.ClientName;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomAce;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomName;
import com.alfresco.consulting.kpmg.meredith.webscript.repository.FolderRepository;

import org.alfresco.repo.model.Repository;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;
import org.springframework.extensions.webscripts.WebScriptException;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FolderRepositoryImpl implements FolderRepository {

    //Dependencies
    private ServiceRegistry serviceRegistry;
    private Repository repositoryHelper;

    public Repository getRepositoryHelper() {
		return repositoryHelper;
	}

	public void setRepositoryHelper(Repository repositoryHelper) {
		this.repositoryHelper = repositoryHelper;
	}

	private Logger logger = Logger.getLogger(FolderRepositoryImpl.class);

    public ServiceRegistry getServiceRegistry() {
        return serviceRegistry;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    //Alfresco Services
    public SearchService getSearchService(){
        return serviceRegistry.getSearchService();
    }

    public FileFolderService getFileFolderService() {
        return serviceRegistry.getFileFolderService();
    }

    public NodeService getNodeService() {
        return serviceRegistry.getNodeService();
    }

    public PermissionService getPermissionService() {
        return serviceRegistry.getPermissionService();
    }
    
    public AuthorityService getAuthorityService() {
        return serviceRegistry.getAuthorityService();
    }
    

    public FolderRepositoryImpl() {
    }

    @Override
    public NodeRef findDataRoomsRoot() {
    	
    	NodeRef companyHome = repositoryHelper.getCompanyHome();
    	NodeRef meredithHome = getFileFolderService().searchSimple(companyHome, "Meredith");
    	NodeRef dataRoomHome = getFileFolderService().searchSimple(meredithHome, "Data Rooms");
    	return dataRoomHome;
    }

    /**
     * Attempts to handle the eventual consistency of the Solr index and search service.
     * @param parent
     * @param name
     * @return
     */
    private NodeRef safeCreateFolder(NodeRef parent, String name){
        NodeRef result;

        try {
            final FileInfo folder = getFileFolderService().create(
                    parent,
                    name,
                    MeredithModel.TYPE_MRD_FOLDER);

            result = folder.getNodeRef();

        } catch (FileExistsException fileExistsException){
            result = getFileFolderService().searchSimple(parent, name);
        }

        return result;
    }

    @Override
    public NodeRef createClientFolder(NodeRef dataRoomsRootNodeRef, ClientName clientName) {

        final NodeRef clientFolderNodeRef = safeCreateFolder(dataRoomsRootNodeRef, clientName.getValue());

        //Add the Meredith Client Aspect
        final Map<QName, Serializable> theProperties = new HashMap<>();
        theProperties.put(MeredithModel.PROP_MRD_CLIENT_NAME, clientName.getValue());

        getNodeService().addAspect(clientFolderNodeRef, MeredithModel.ASPECT_MRD_CLIENT, theProperties);

        return clientFolderNodeRef;
    }

    @Override
    public NodeRef createDataRoomFolder(NodeRef clientNodeRef, DataRoomName dataRoomName, List<DataRoomAce> thePermissions) {
        NodeRef theDataRoomFolder = createFolderInWithAspect(clientNodeRef, dataRoomName.getValue(), MeredithModel.ASPECT_MRD_DATA_ROOM);
        
        final AuthorityService theAuthorityService = getAuthorityService();
        
        if (thePermissions != null) {
	        //Alfresco Roles
			//TODO - Throws NPE when String[] declared as a constant?
			final String[] ALFRESCO_ROLES = {PermissionService.CONSUMER, 
									   		 PermissionService.CONTRIBUTOR,
									   		 PermissionService.COORDINATOR,
									   		 PermissionService.EDITOR,
									   		 "Collaborator"};
			final Set<String> validRoles = new HashSet<String>(Arrays.asList(ALFRESCO_ROLES));
	
	        for(DataRoomAce seedPermission : thePermissions) {
	        	
	    		//Validate the Role
	    		if (!validRoles.contains(seedPermission.getRole())) {
	    			throw new WebScriptException("An Invalid Role has been provided: " + seedPermission.getRole());
	    		}
	    		
	    		//Validate the Principal
	    		if (!theAuthorityService.authorityExists(seedPermission.getPrincipal())) {
	    			throw new WebScriptException("An Invalid Principal has been provided: " + seedPermission.getPrincipal());
	    		}
	    		
	            getPermissionService().setPermission(
	            		theDataRoomFolder,
	                    seedPermission.getPrincipal(),
	                    seedPermission.getRole(),
	                    true // allow, rather than deny
	            );
	        }
        }
        return theDataRoomFolder;
    }

    @Override
    public NodeRef createDataRoomSubFolder(NodeRef dataRoomNodeRef, String name) {
    	
        NodeRef subFolder = createFolderIn(dataRoomNodeRef, name);
        return subFolder;
    }

    private NodeRef createFolderIn(NodeRef parent, String folderName) {
        return createFolderInWithAspect(parent, folderName, null);
    }

    private NodeRef createFolderInWithAspect(NodeRef parent, String folderName, QName aspect){
        logger.debug("Creating Folder: " + folderName);

        final NodeRef folderNodeRef = safeCreateFolder(parent, folderName);

        if(aspect != null) getNodeService().addAspect(folderNodeRef, aspect, null);

        return folderNodeRef;
    }

}
