package com.alfresco.consulting.kpmg.meredith.webscript.dataroom;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.log4j.Logger;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;

import com.alfresco.consulting.kpmg.meredith.action.MeredithIndexActionExecuter;
import com.alfresco.consulting.kpmg.meredith.model.MeredithModel;
import com.alfresco.consulting.kpmg.meredith.webscript.builder.DataRoomIndexRequestBuilder;
import com.alfresco.consulting.kpmg.meredith.webscript.builder.DataRoomIndexResponseBuilder;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomIndexRequest;
import com.alfresco.consulting.kpmg.meredith.webscript.repository.FolderRepository;

public class DataRoomIndex extends DeclarativeWebScript {

	//Logger
    private Logger logger = Logger.getLogger(DataRoomIndex.class);
    
	//Alfresco Services
	private NodeService theNodeService;
	private ActionService theActionService;

    //Dependencies
	private ServiceRegistry serviceRegistry;
	
	public ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}
	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}
	
	//Alfresco Services
	private FolderRepository folderRepository;

	public FolderRepository getFolderRepository() {
		return folderRepository;
	}

	public void setFolderRepository(FolderRepository folderRepository){
		this.folderRepository = folderRepository;
	}

	protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache){
		
		//Log the Method Invocation
		final DataRoomIndexRequest theParameters = DataRoomIndexRequestBuilder.build(req);
		logger.info("Indexing Meredith Data Room: " + theParameters.getClientName().getValue() + " -> " + theParameters.getDataRoomName().getValue());
		
		//Obtain the Alfresco Services
		theNodeService = serviceRegistry.getNodeService();
		theActionService = serviceRegistry.getActionService();
		
		//Obtain the Data Room Home
		NodeRef theDataRoomHome = folderRepository.findDataRoomsRoot();
		if (theDataRoomHome == null) {
			throw new WebScriptException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Meredith Data Rooms Home does not exist");
		}
		
		//Obtain the Client Home
		final NodeRef theClientHome = serviceRegistry.getFileFolderService().searchSimple(theDataRoomHome, theParameters.getClientName().getValue());
		if (theClientHome == null) {
			throw new WebScriptException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Client Home Folder does not exist: " + theParameters.getClientName().getValue());
		}
		
		//Obtain the Client Data Room
		final NodeRef theClientDataRoomHome = serviceRegistry.getFileFolderService().searchSimple(theClientHome, theParameters.getDataRoomName().getValue());
		if (theClientDataRoomHome == null) {
			throw new WebScriptException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Client Data Room Folder does not exist " + theParameters.getDataRoomName().getValue());
		}
		
		//Obtain the Documents Folder from the Data Room
		final NodeRef theDocuments = serviceRegistry.getFileFolderService().searchSimple(theClientDataRoomHome, "Documents");
		if (theDocuments == null) {
			throw new WebScriptException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Documents Folder does not exist");
		}
		
		//Execute the Index Action
		if (theNodeService.hasAspect(theClientDataRoomHome, MeredithModel.ASPECT_MRD_DATA_ROOM)) {
			
			//Create the Action Parameters
			final Map<String, Serializable> theActionParameters = new HashMap<String, Serializable>();
			theActionParameters.put(MeredithIndexActionExecuter.PARAM_RECURSION, true);

			//Create the Action
			final Action theIndexAction = theActionService.createAction(MeredithIndexActionExecuter.NAME, theActionParameters);
			theActionService.executeAction(theIndexAction, theDocuments, false, true);
			
		} else {
			
			//Provided Node Reference is not a Data Room
			throw new WebScriptException(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Folder identified by: " + theParameters.getDataRoomName().getValue() + " is not a Meredith Data Room");
		}
						
		//Construct the Response
		return DataRoomIndexResponseBuilder.build(theParameters.getDataRoomName().getValue());
	}
}

