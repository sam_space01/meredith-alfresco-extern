package com.alfresco.consulting.kpmg.meredith.webscript.model;

public class DataRoomAce {
    private final String principal;
    private final String role;

    public DataRoomAce(String principal, String role) {
        this.principal = principal;
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public String getPrincipal() {
        return principal;
    }
}
