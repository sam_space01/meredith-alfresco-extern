package com.alfresco.consulting.kpmg.meredith.webscript.model;

/**
 * Encapsulates the inputs to the Index Data Room webscript.
 */
public class DataRoomIndexRequest {
    private final String clientName;
    private final String dataRoomName;

    public DataRoomIndexRequest(String clientName, String dataRoomName) {
        this.clientName = clientName;
        this.dataRoomName = dataRoomName;
    }

    public ClientName getClientName() {
        return new ClientName(clientName);
    }

    public DataRoomName getDataRoomName() {
        return new DataRoomName(dataRoomName);
    }
}
