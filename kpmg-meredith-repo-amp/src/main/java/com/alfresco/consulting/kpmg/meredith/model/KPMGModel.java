package com.alfresco.consulting.kpmg.meredith.model;

import org.alfresco.service.namespace.QName;

/**
 * KPMG Model Constants
 */
public interface KPMGModel
{
    /*
     * KPMG Model URI
     */
    static final String KPMG_MODEL_1_0_URI = "http://www.kpmg.com/alfresco/1.0";

    /* 
     * KPMG Model Prefix
     */
    static final String KPMG_MODEL_PREFIX = "kpmg";
	
    /*
     *  KPMG Content - Definitions
     */
    static final QName TYPE_KPMG_CONTENT = QName.createQName(KPMG_MODEL_1_0_URI, "content");
    static final QName TYPE_KPMG_FOLDER = QName.createQName(KPMG_MODEL_1_0_URI, "folder");
    
}
