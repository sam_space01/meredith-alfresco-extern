package com.alfresco.consulting.kpmg.meredith.audit;

import java.io.Serializable;

import org.alfresco.repo.audit.extractor.AbstractDataExtractor;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.util.PropertyCheck;

/**
 * An extractor to pull the Principal Display Name from a Group.
 */
public class PrincipalNameDataExtractor extends AbstractDataExtractor {
	
	//Audit Suffixes
    private static final String AUDIT_SUFFIX_USER = " (User)";
	private static final String AUDIT_SUFFIX_GROUP = " (Group)";
	
	//Alfresco Services
	private AuthorityService authorityService;

    public void setAuthorityService(AuthorityService authorityService)
    {
        this.authorityService = authorityService;
    }
    
    @Override
    public void afterPropertiesSet() throws Exception
    {
        super.afterPropertiesSet();
        PropertyCheck.mandatory(this, "authorityService", authorityService);
    }

    public boolean isSupported(Serializable data)
    {
        return (data != null && data instanceof String);
    }

    public Serializable extractData(Serializable in) throws Throwable
    {
        String principalId = (String) in;
        String principalName = null;
        
        if (!authorityService.authorityExists(principalId))
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Extractor can't pull value from non-existent group: " + principalId);
            }
        }
        else
        {
        	//Get the Principal Name
        	if (principalId.startsWith(PermissionService.GROUP_PREFIX))
        		principalName = authorityService.getAuthorityDisplayName(principalId) + AUDIT_SUFFIX_GROUP;
        	else {
        		principalName = principalId + AUDIT_SUFFIX_USER;
        	}
         
        }
        return principalName;
    }
}
