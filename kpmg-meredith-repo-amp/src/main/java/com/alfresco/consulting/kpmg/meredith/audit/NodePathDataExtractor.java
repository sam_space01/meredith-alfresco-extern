package com.alfresco.consulting.kpmg.meredith.audit;

import java.io.Serializable;

import org.alfresco.repo.audit.extractor.AbstractDataExtractor;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.util.PropertyCheck;
import org.alfresco.model.ContentModel;

/**
 * An extractor to pull the path from a Node.
 */
public class NodePathDataExtractor extends AbstractDataExtractor
{
    private NodeService nodeService;
    private PermissionService permissionService;
    
    public void setNodeService(NodeService nodeService)
    {
        this.nodeService = nodeService;
    }
    
    public void setPermissionService(PermissionService permissionService)
    {
        this.permissionService = permissionService;
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        super.afterPropertiesSet();
        PropertyCheck.mandatory(this, "nodeService", nodeService);
    }

    public boolean isSupported(Serializable data)
    {
        return (data != null && data instanceof NodeRef);
    }

    public Serializable extractData(Serializable in) throws Throwable
    {
        NodeRef nodeRef = (NodeRef) in;
        Path nodePath = null;
        String nodeName = null;
        if (!nodeService.exists(nodeRef))
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Extractor can't pull value from non-existent node: " + nodeRef);
            }
        }
        else
        {
        	//Get the Node Path
            nodePath = nodeService.getPath(nodeRef);
            
            //Get the Node Name
            nodeName = (String) nodeService.getProperty(nodeRef, ContentModel.PROP_NAME);
         
        }
        return (nodePath.toDisplayPath(nodeService, permissionService) + "/" + nodeName);
    }
}
