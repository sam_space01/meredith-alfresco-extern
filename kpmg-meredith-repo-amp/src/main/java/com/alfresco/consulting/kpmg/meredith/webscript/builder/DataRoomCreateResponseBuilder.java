package com.alfresco.consulting.kpmg.meredith.webscript.builder;

import org.alfresco.service.cmr.repository.NodeRef;

import java.util.HashMap;
import java.util.Map;

public class DataRoomCreateResponseBuilder {

    private static final String RESPONSE_DOCUMENTS_FOLDER = "documentsFolder";
    private static final String RESPONSE_DATA_ROOM_FOLDER = "dataRoomFolder";
    private static final String RESPONSE_CLIENT_FOLDER = "clientFolder";
    private static final String RESPONSE_BIDS_FOLDER = "bidsFolder";

    public static Map<String, Object> build(NodeRef theClientFolder,
                                            NodeRef theDataRoomFolder,
                                            NodeRef theDocumentsFolder,
                                            NodeRef theeBidsFolder) {
        //Construct the Response
        final Map<String, Object> theModel = new HashMap<String, Object>();
        theModel.put(RESPONSE_CLIENT_FOLDER, theClientFolder.getId());
        theModel.put(RESPONSE_DATA_ROOM_FOLDER, theDataRoomFolder.getId());
        theModel.put(RESPONSE_DOCUMENTS_FOLDER, theDocumentsFolder.getId());
        theModel.put(RESPONSE_BIDS_FOLDER, theeBidsFolder.getId());
        return theModel;
    }

}
