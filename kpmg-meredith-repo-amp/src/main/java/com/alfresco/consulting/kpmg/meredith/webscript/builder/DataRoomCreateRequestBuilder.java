package com.alfresco.consulting.kpmg.meredith.webscript.builder;

import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomCreateRequest;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomAce;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

public class DataRoomCreateRequestBuilder {
    private static Logger logger = Logger.getLogger(DataRoomCreateRequestBuilder.class);

    //WebScript Request Parameters
    public static final String PARAM_PERMISSION_ROLE = "role";
    public static final String PARAM_PERMISSION_PRINCIPAL = "principal";
    public static final String PARAM_DATA_ROOM_NAME = "dataRoomName";
    public static final String PARAM_CLIENT_NAME = "clientName";
    public static final String PARAM_PERMISSIONS = "permissions";

    /**
     * Obtains the Parameters from the Request
     *
     * Extract the Parameters from the Request
     {
     "clientName":"clientName",
     "dataRoomName":"dataRoomName",
     "permissions": [
     {"principal":"GROUP_1", "role":"Consumer"},
     {"principal":"GROUP_2", "role":"Contributor"}
     ]
     }
     * @param 	theRequest
     * @return	Map<String,Object>
     * 				the Webscript Parameters
     */
    public static DataRoomCreateRequest build(final WebScriptRequest theRequest) {
        //Log the Method Invocation
        logger.debug("Obtaining Parameters from Request: " + theRequest.getURL());

        //Obtain the JSON from the Request
        final Object theJSONObject = theRequest.parseContent();

        //Validate the JSON
        if (theJSONObject == null || !(theJSONObject instanceof JSONObject)) {
            throw new WebScriptException(HttpServletResponse.SC_BAD_REQUEST, "Invalid Request");
        }

        return build((JSONObject)theJSONObject);
    }

    public static DataRoomCreateRequest build(final JSONObject theJSON) {

        try {
            //Extract the Parameters
            final String theClientName = extractStringFrom(theJSON, PARAM_CLIENT_NAME);
            final String theDataRoomName = extractStringFrom(theJSON, PARAM_DATA_ROOM_NAME);
            final JSONArray thePermissionsJson = extractListFrom(theJSON, PARAM_PERMISSIONS);
            final ArrayList<DataRoomAce> thePermissions = toCreateDataRoomPermissions(thePermissionsJson);

            return new DataRoomCreateRequest(theClientName, theDataRoomName, thePermissions);

        } catch (JSONException e) {
            throw new WebScriptException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        }
    }

    public static String extractStringFrom(final JSONObject theJSON, String theFieldName) throws JSONException {
        final String theValue = theJSON.getString(theFieldName);

        if (theValue == null || theValue.isEmpty()){
            throw new WebScriptException("Parameter: " + theFieldName + " has not been provided");
        }

        return theValue;
    }

    public static JSONArray extractListFrom(final JSONObject theJSON, String theFieldName) throws JSONException {
        if(theJSON.has(theFieldName)) return theJSON.getJSONArray(theFieldName);

        return new JSONArray();
    }

    public static ArrayList<DataRoomAce> toCreateDataRoomPermissions(JSONArray theACEs) throws JSONException {
        ArrayList<DataRoomAce> perms = new ArrayList<>();

        for (int i = 0; i < theACEs.length(); i++){
            final JSONObject theACE = theACEs.getJSONObject(i);
            final DataRoomAce perm = toCreateDataRoomPermission(theACE);
            perms.add(perm);
        }

        return perms;
    }

    public static DataRoomAce toCreateDataRoomPermission(JSONObject theACE) throws JSONException {
        String principal = theACE.getString(PARAM_PERMISSION_PRINCIPAL);
        String role = theACE.getString(PARAM_PERMISSION_ROLE);

        return new DataRoomAce(principal, role);
    }
}
