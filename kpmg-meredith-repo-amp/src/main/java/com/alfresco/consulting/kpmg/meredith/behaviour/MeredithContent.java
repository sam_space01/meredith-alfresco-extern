package com.alfresco.consulting.kpmg.meredith.behaviour;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.Behaviour.NotificationFrequency;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ActionService;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.log4j.Logger;

import com.alfresco.consulting.kpmg.meredith.action.MeredithIndexActionExecuter;
import com.alfresco.consulting.kpmg.meredith.model.MeredithModel;

public class MeredithContent implements NodeServicePolicies.OnCreateNodePolicy, NodeServicePolicies.OnSetNodeTypePolicy {
	
    //Logger
    private Logger logger = Logger.getLogger(MeredithContent.class);
    
    //Behaviours
    private Behaviour onCreateNode;
    private Behaviour onSetNodeType;

    //Policies
	private PolicyComponent policyComponent;

	//Dependencies
    private ServiceRegistry serviceRegistry;
    
    //Alfresco Services
    private ActionService actionService;
    
    //Spring Getters / Setters
    public ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}
	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}
	public PolicyComponent getPolicyComponent() {
		return policyComponent;
	}
	public void setPolicyComponent(PolicyComponent policyComponent) {
		this.policyComponent = policyComponent;
	}

    /**
     * Initialise the Meredith Content Behaviour
     */
    public void init() {
    	
    	//Obtain the Alfresco Services
    	actionService = serviceRegistry.getActionService();
    	
        //Create the Meredith Content Behaviour
    	logger.info("Creating Meredith Content Behaviour ...");
        this.onCreateNode = new JavaBehaviour(this, "onCreateNode", NotificationFrequency.TRANSACTION_COMMIT);
        this.onSetNodeType = new JavaBehaviour(this, "onSetNodeType", NotificationFrequency.TRANSACTION_COMMIT);

        //Bind Behaviours
        logger.info("Binding Policies to Meredith Content Behaviour ...");
        this.policyComponent.bindClassBehaviour(QName.createQName(NamespaceService.ALFRESCO_URI, "onCreateNode"), MeredithModel.TYPE_MRD_CONTENT, this.onCreateNode);
        this.policyComponent.bindClassBehaviour(QName.createQName(NamespaceService.ALFRESCO_URI, "onSetNodeType"), ContentModel.TYPE_CONTENT, this.onSetNodeType);
    }
    
    @Override
	public void onCreateNode(ChildAssociationRef childAssocRef) {
    	
    	//Add Index if creating Meredith Content
    	logger.debug("Meredith Content - On Create Node: " + childAssocRef.getChildRef());
    	indexNode(childAssocRef.getChildRef());
	}
    
	@Override
	public void onSetNodeType(NodeRef nodeRef, QName oldType, QName newType) {
		
		//Add Index if specialised to Meredith Content
		if(newType.equals(MeredithModel.TYPE_MRD_CONTENT)){
			logger.debug("Meredith Content - On Set Node Type: " + nodeRef);
			indexNode(nodeRef);	
		}
	}
	
	/**
	 * Indexes the Meredith Node
	 * @param nodeRef
	 * 			the NodeRef
	 */
	private void indexNode(final NodeRef nodeRef){

		//Create the Action Parameters - Recursion Not Applicable for Content
		final Map<String, Serializable> theActionParameters = new HashMap<String, Serializable>();
		theActionParameters.put(MeredithIndexActionExecuter.PARAM_RECURSION, false);

		//Create the Action - Asynchronous Not Applicable for Content
		final Action theIndexAction = actionService.createAction(MeredithIndexActionExecuter.NAME, theActionParameters);
		actionService.executeAction(theIndexAction, nodeRef, false, false);
	}

}