package com.alfresco.consulting.kpmg.meredith.webscript.builder;

import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomCreateRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestDataRoomCreateRequestBuilder {

    JSONObject validInput;
    private final String clientName = "testClientName";
    private final String dataRoomName = "testDataRoom";

    @Before
    public void setup() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put(DataRoomCreateRequestBuilder.PARAM_CLIENT_NAME, clientName);
        obj.put(DataRoomCreateRequestBuilder.PARAM_DATA_ROOM_NAME, dataRoomName);

        JSONArray permissions = new JSONArray();

        JSONObject perm1 = new JSONObject();
        perm1.put(DataRoomCreateRequestBuilder.PARAM_PERMISSION_PRINCIPAL, "Principal1");
        perm1.put(DataRoomCreateRequestBuilder.PARAM_PERMISSION_ROLE, "Role1");

        JSONObject perm2 = new JSONObject();
        perm2.put(DataRoomCreateRequestBuilder.PARAM_PERMISSION_PRINCIPAL, "Principal2");
        perm2.put(DataRoomCreateRequestBuilder.PARAM_PERMISSION_ROLE, "Role2");

        permissions.put(0, perm1);
        permissions.put(1, perm2);
        obj.put(DataRoomCreateRequestBuilder.PARAM_PERMISSIONS, permissions);

        validInput = obj;
    }

    @Test
    public void testInputParamExtraction() {
        DataRoomCreateRequest params = DataRoomCreateRequestBuilder.build(validInput);

        assertEquals(clientName, params.getClientName().getValue());
        assertEquals(dataRoomName, params.getDataRoomName().getValue());
        assertEquals(2, params.getSeedAcl().size());
        assertEquals("Principal1", params.getSeedAcl().get(0).getPrincipal());
        assertEquals("Role1", params.getSeedAcl().get(0).getRole());
        assertEquals("Principal2", params.getSeedAcl().get(1).getPrincipal());
        assertEquals("Role2", params.getSeedAcl().get(1).getRole());
    }
}
