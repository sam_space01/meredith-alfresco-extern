package com.alfresco.consulting.kpmg.meredith.webscript.dataroom;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.transaction.UserTransaction;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.model.Repository;
import org.alfresco.repo.security.authentication.AuthenticationComponent;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.transaction.TransactionService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alfresco.consulting.kpmg.meredith.service.dataroom.MeredithDataRoomException;
import com.alfresco.consulting.kpmg.meredith.service.dataroom.MeredithDataRoomService;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomAce;
import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomCreateRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:alfresco/application-context.xml"})
public class TestDataRoomCreate {
    
	//Permission Constants
	private static final String VALID_USER = "abeecher";
	private static final String INVALID_USER = "Invalid User";
	private static final String VALID_ROLE = "Consumer";
	private static final String INVALID_ROLE = "Invalid Role";
	
	//Folder Constants
	private static final String EMPTY_STRING = "";
	private static final String MEREDITH_HOME = "Meredith";
	private static final String DATA_ROOMS_HOME = "Data Rooms";
	private static final String VALID_CLIENT_NAME = "KPMG";
	private static final String VALID_DATA_ROOM_NAME = "Exchange";
	private static final String BIDS_FOLDER = "Bids";
	private static final String DOCUMENTS_FOLDER = "Documents";
	
	@Autowired
	private NodeService nodeService;
	
	@Autowired
	private SearchService searchService;
	
	@Autowired
	private FileFolderService fileFolderService;
	
	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private TransactionService transactionService;
	
	@Autowired
    private MeredithDataRoomService meredithDataRoomService;
	
	@Autowired
	protected AuthenticationComponent authenticationComponent;
	
	@Autowired
	private Repository repositoryHelper;
	
	/**
	 * Alfresco Company Home
	 */
	private NodeRef companyHome;
	
	/**
	 * Meredith Home (Bootstrapped)
	 */
	private NodeRef meredithHome;
	
	/**
	 * Data Room Home (Bootstrapped)
	 */
	private NodeRef dataRoomHome;
	
	/**
	 * Setup
	 */
	@Before
	public void setUp() {
		
		//Authenticate the User as Admin
		authenticationComponent.setSystemUserAsCurrentUser();
		
		//Obtain the Company Home
		companyHome = repositoryHelper.getCompanyHome();
		assertNotNull("Company Home is unavailable", companyHome);

        //Validate Meredith Home Bootstrap
		meredithHome = fileFolderService.searchSimple(companyHome, MEREDITH_HOME);
        assertNotNull(meredithHome);
        assertEquals("Meredith Home is unavailable", MEREDITH_HOME, nodeService.getProperty(meredithHome, ContentModel.PROP_NAME));
		
        //Validate Data Room Home Bootstrap
		dataRoomHome = fileFolderService.searchSimple(meredithHome, DATA_ROOMS_HOME);
        assertNotNull(dataRoomHome);
        assertEquals("Data Rooms Home is unavailable", DATA_ROOMS_HOME, nodeService.getProperty(dataRoomHome, ContentModel.PROP_NAME));
	}
	
    /**
     * Test Bean Wiring.
     */
    @Test
    public void testWiring() {
        assertNotNull(nodeService);
        assertNotNull(searchService);
        assertNotNull(fileFolderService);
        assertNotNull(permissionService);
        assertNotNull(transactionService);
        assertNotNull(meredithDataRoomService);
        assertNotNull(authenticationComponent);
        assertNotNull(repositoryHelper);
    }
    
    /**
     * Test Create Data Room - Null Client
     * @throws MeredithDataRoomException 
     */
    @Test (expected=MeredithDataRoomException.class)
    public void testCreateDataRoomNullClient() throws MeredithDataRoomException {
    	
    	//Create the Data Room Request
    	final DataRoomCreateRequest theRequest = new DataRoomCreateRequest(null, VALID_DATA_ROOM_NAME, null);
    	
    	//Create the Data Room
    	meredithDataRoomService.create(theRequest);
    }
    
    /**
     * Test Create Data Room - Empty Client
     */
    @Test (expected=MeredithDataRoomException.class)
    public void testCreateDataRoomEmptyClient() throws MeredithDataRoomException {
    	
    	//Create the Data Room Request
    	final DataRoomCreateRequest theRequest = new DataRoomCreateRequest(EMPTY_STRING, VALID_DATA_ROOM_NAME, null);
    	
    	//Create the Data Room
    	meredithDataRoomService.create(theRequest);
    }
    
    /**
     * Test Create Data Room - Null Data Room
     */
    @Test (expected=MeredithDataRoomException.class)
    public void testCreateDataRoomNullDataRoom() throws MeredithDataRoomException {
    	
    	//Create the Data Room Request
    	final DataRoomCreateRequest theRequest = new DataRoomCreateRequest(VALID_CLIENT_NAME, null, null);
    	
    	//Create the Data Room
    	meredithDataRoomService.create(theRequest);
    }
    
    /**
     * Test Create Data Room - Empty DataRoom
     */
    @Test (expected=MeredithDataRoomException.class)
    public void testCreateDataRoomEmptyDataRoom() throws MeredithDataRoomException {
    	
    	//Create the Data Room Request
    	final DataRoomCreateRequest theRequest = new DataRoomCreateRequest(VALID_CLIENT_NAME, EMPTY_STRING, null);
    	
    	//Create the Data Room
    	meredithDataRoomService.create(theRequest);
    }
	
    /**
     * Test Create Data Room - No Permissions Provided.
     */
    @Test
    public void testCreateDataRoomWithoutPermissions() throws MeredithDataRoomException {
    	
    	//Create the Data Room Request
    	final DataRoomCreateRequest theRequest = new DataRoomCreateRequest(VALID_CLIENT_NAME, VALID_DATA_ROOM_NAME, null);
    	
    	//Create the Data Room
    	meredithDataRoomService.create(theRequest);
    	
    	//Validate the Client Home
    	final NodeRef clientHome = fileFolderService.searchSimple(dataRoomHome, VALID_CLIENT_NAME);
    	assertNotNull(clientHome);
    	assertEquals("Client Home Folder does not exist", VALID_CLIENT_NAME, nodeService.getProperty(clientHome, ContentModel.PROP_NAME));
    	
    	//Validate the Client Data Room
    	final NodeRef clientDataRoomHome = fileFolderService.searchSimple(clientHome, VALID_DATA_ROOM_NAME);
    	assertNotNull(clientDataRoomHome);
    	assertEquals("Data Room Home Folder does not exist", VALID_DATA_ROOM_NAME, nodeService.getProperty(clientDataRoomHome, ContentModel.PROP_NAME));
    	
    	//Validate the Bid Folder
    	final NodeRef bidHome = fileFolderService.searchSimple(clientDataRoomHome, BIDS_FOLDER);
    	assertNotNull("Bid Folder does not exist", bidHome);
    	
    	//Validate the Documents Folder
    	final NodeRef documentsHome = fileFolderService.searchSimple(clientDataRoomHome, DOCUMENTS_FOLDER);
    	assertNotNull("Documents Folder does not exist", documentsHome);
    }
    
    /**
     * Test Create Data Room - No Permissions Provided, Attempt to Create a Duplicate.
     */
    @Test
    public void testCreateDataRoomWithoutPermissionsDuplicate() throws MeredithDataRoomException {
    	
    	//Create the Data Room Request
    	final DataRoomCreateRequest theRequest = new DataRoomCreateRequest(VALID_CLIENT_NAME, VALID_DATA_ROOM_NAME, null);
    	
    	//Create the Data Room
    	meredithDataRoomService.create(theRequest);
    	
    	//Validate the Client Home
    	final NodeRef clientHome = fileFolderService.searchSimple(dataRoomHome, VALID_CLIENT_NAME);
    	assertNotNull(clientHome);
    	assertEquals("Client Home Folder does not exist", VALID_CLIENT_NAME, nodeService.getProperty(clientHome, ContentModel.PROP_NAME));
    	
    	//Validate the Client Data Room
    	final NodeRef clientDataRoomHome = fileFolderService.searchSimple(clientHome, VALID_DATA_ROOM_NAME);
    	assertNotNull(clientDataRoomHome);
    	assertEquals("Data Room Home Folder does not exist", VALID_DATA_ROOM_NAME, nodeService.getProperty(clientDataRoomHome, ContentModel.PROP_NAME));
    	
    	//Validate the Bid Folder
    	final NodeRef bidHome = fileFolderService.searchSimple(clientDataRoomHome, BIDS_FOLDER);
    	assertNotNull("Bid Folder does not exist", bidHome);
    	
    	//Validate the Documents Folder
    	final NodeRef documentsHome = fileFolderService.searchSimple(clientDataRoomHome, DOCUMENTS_FOLDER);
    	assertNotNull("Documents Folder does not exist", documentsHome);
    	
    	//Create the Data Room Duplicate
    	meredithDataRoomService.create(theRequest);
    	
    	//Validate the Client Home
    	final NodeRef clientHomeDuplicate = fileFolderService.searchSimple(dataRoomHome, VALID_CLIENT_NAME);
    	assertEquals(clientHome, clientHomeDuplicate);
    	
    	//Validate the Client Data Room
    	final NodeRef clientDataRoomHomeDuplicate = fileFolderService.searchSimple(clientHome, VALID_DATA_ROOM_NAME);
    	assertEquals(clientDataRoomHome, clientDataRoomHomeDuplicate);
    	assertEquals("Data Room Home Folder does not exist", VALID_DATA_ROOM_NAME, nodeService.getProperty(clientDataRoomHome, ContentModel.PROP_NAME));
    	
    	//Validate the Bid Folder
    	final NodeRef bidHomeDuplicate = fileFolderService.searchSimple(clientDataRoomHome, BIDS_FOLDER);
    	assertEquals(bidHome, bidHomeDuplicate);
    	
    	//Validate the Documents Folder
    	final NodeRef documentsHomeDuplicate = fileFolderService.searchSimple(clientDataRoomHome, DOCUMENTS_FOLDER);
    	assertEquals(documentsHome, documentsHomeDuplicate);
    }
    
    /**
     * Test Create Data Room - Permissions Provided - Invalid Principal.
     */
    @Test (expected=WebScriptException.class)
    public void testCreateDataRoomWithPermissionsInvalidPrincipal() throws MeredithDataRoomException {
    	
    	//Create the Data Room Permissions
    	final List<DataRoomAce> thePermissions = new ArrayList<DataRoomAce>();
    	thePermissions.add(new DataRoomAce(INVALID_USER, VALID_ROLE));
    	
    	//Create the Data Room Request
    	final DataRoomCreateRequest theRequest = new DataRoomCreateRequest(VALID_CLIENT_NAME, VALID_DATA_ROOM_NAME, thePermissions);
    	
    	//Create the Data Room
    	meredithDataRoomService.create(theRequest);
    }
    
    /**
     * Test Create Data Room - Permissions Provided - Invalid Role.
     */
    @Test (expected=WebScriptException.class)
    public void testCreateDataRoomWithPermissionsInvalidRole() throws MeredithDataRoomException {
    	
    	//Create the Data Room Request
    	final List<DataRoomAce> thePermissions = new ArrayList<DataRoomAce>();
    	thePermissions.add(new DataRoomAce(VALID_USER, INVALID_ROLE));
    	
    	//Create the Data Room Request
    	final DataRoomCreateRequest theRequest = new DataRoomCreateRequest(VALID_CLIENT_NAME, VALID_DATA_ROOM_NAME, thePermissions);
    	
    	//Create the Data Room
    	meredithDataRoomService.create(theRequest);
    }
    
    /**
     * Test Create Data Room - Permissions Provided.
     */
    @Test
    public void testCreateDataRoomWithPermissions() throws MeredithDataRoomException {
    	
    	//Create the Data Room Request
    	final List<DataRoomAce> thePermissions = new ArrayList<DataRoomAce>();
    	thePermissions.add(new DataRoomAce(VALID_USER, VALID_ROLE));
    	
    	//Create the Data Room Request
    	final DataRoomCreateRequest theRequest = new DataRoomCreateRequest(VALID_CLIENT_NAME, VALID_DATA_ROOM_NAME, thePermissions);
    	
    	//Create the Data Room
    	meredithDataRoomService.create(theRequest);
    	
    	//Validate the Client Home
    	final NodeRef clientHome = fileFolderService.searchSimple(dataRoomHome, VALID_CLIENT_NAME);
    	assertNotNull(clientHome);
    	assertEquals("Client Home Folder does not exist", VALID_CLIENT_NAME, nodeService.getProperty(clientHome, ContentModel.PROP_NAME));
    	
    	//Validate the Client Data Room
    	final NodeRef clientDataRoomHome = fileFolderService.searchSimple(clientHome, VALID_DATA_ROOM_NAME);
    	assertNotNull(clientDataRoomHome);
    	assertEquals("Data Room Home Folder does not exist", VALID_DATA_ROOM_NAME, nodeService.getProperty(clientDataRoomHome, ContentModel.PROP_NAME));
    	
    	//Validate the Bid Folder
    	final NodeRef bidHome = fileFolderService.searchSimple(clientDataRoomHome, BIDS_FOLDER);
    	assertNotNull("Bid Folder does not exist", bidHome);
    	
    	//Validate the Documents Folder
    	final NodeRef documentsHome = fileFolderService.searchSimple(clientDataRoomHome, DOCUMENTS_FOLDER);
    	assertNotNull("Documents Folder does not exist", documentsHome);
    	    	
    	//Validate the Client Data Room Permissions
    	AccessPermission actualPermission = null;
    	final Set<AccessPermission> theConfiguredPermissions = permissionService.getAllSetPermissions(clientDataRoomHome);
    	for (AccessPermission theAccessPermission : theConfiguredPermissions){
    		if (theAccessPermission.getAuthority().equals(VALID_USER)) {
    			actualPermission = theAccessPermission;
    		}
    	}
    	
    	//Validate the Permission
    	assertNotNull(actualPermission);
    	assertEquals(VALID_ROLE, actualPermission.getPermission());
    	assertEquals(VALID_USER, actualPermission.getAuthority());
    }
    
    /**
     * Teardown.
     */
    @After
    public void tearDown() {
    	
		UserTransaction tx = null;
		try {

			//Start the Transaction
			tx = transactionService.getUserTransaction();
			tx.begin();
		
			//Delete Test Folders
			final List<FileInfo> theFolders = fileFolderService.list(dataRoomHome);
			for (FileInfo theFolder : theFolders){
				fileFolderService.delete(theFolder.getNodeRef());
			}

			//Commit the Transaction
			tx.commit();
		
		} catch (Throwable err) {
			
			//Handle the Error
			try { if (tx != null) {tx.rollback();} } catch (Exception tex) {}
		}
		
		//Clear the Authentication
		authenticationComponent.clearCurrentSecurityContext();
    }
}
