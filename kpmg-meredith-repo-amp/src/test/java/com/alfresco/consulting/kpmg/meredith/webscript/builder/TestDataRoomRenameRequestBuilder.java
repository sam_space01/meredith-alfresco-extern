package com.alfresco.consulting.kpmg.meredith.webscript.builder;

import com.alfresco.consulting.kpmg.meredith.webscript.model.DataRoomRenameRequest;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestDataRoomRenameRequestBuilder {

    JSONObject validInput;
    private final String clientName = "testClientName";
    private final String prevDataRoomName = "testDataRoom";
    private final String newDataRoomName = "testDataRoom 303";

    @Before
    public void setup() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("clientName", clientName);
        obj.put("currentDataRoomName", prevDataRoomName);
        obj.put("newDataRoomName", newDataRoomName);

        validInput = obj;
    }

    @Test
    public void testInputParamExtraction() {
        DataRoomRenameRequest params = DataRoomRenameRequestBuilder.build(validInput);

        assertEquals(clientName, params.getClientName().getValue());
        assertEquals(newDataRoomName, params.getNewDataRoomName().getValue());
        assertEquals(prevDataRoomName, params.getPreviousDataRoomName().getValue());
    }
}