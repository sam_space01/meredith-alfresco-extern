package com.alfresco.consulting.kpmg.meredith.model.workflow.qa;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class TestQaWorkflow {

	//Obtain a Logger
    Logger logger = LoggerFactory.getLogger(TestQaWorkflow.class);
    
	@Rule
	public ActivitiRule theActivitiRule = new ActivitiRule("activiti.cfg.mem-fullhistory.xml");
	
	//Meredith QA Process Constants
	private static final String PROCESS_ID = "workflow-meredith-qa";
	private static final String PROCESS_XML = "workflow-meredith-qa.bpmn";
	
	//Meredith QA User Constants
	private static final String USER_EXCHANGE_1_BUYER_1_USER_1 = "exchange_1_buyer_1_user_1";
	private static final String USER_EXCHANGE_1_BUYER_1_USER_2 = "exchange_1_buyer_1_user_2";
	private static final String USER_EXCHANGE_1_BUYER_1_LEAD_BUYER_1 = "exchange_1_buyer_1_lead_buyer_1";
	private static final String USER_EXCHANGE_1_BUYER_1_LEAD_BUYER_2 = "exchange_1_buyer_1_lead_buyer_2";
	private static final String USER_EXCHANGE_1_LEAD_SELLER_1 = "exchange_1_lead_seller_1";
	private static final String USER_EXCHANGE_1_LEAD_SELLER_2 = "exchange_1_lead_seller_2";
	private static final String USER_EXCHANGE_1_SELLER_SME_1 = "exchange_1_seller_sme_1";
	private static final String USER_EXCHANGE_1_SELLER_SME_2 = "exchange_1_seller_sme_2";
	
	//Meredith QA Group Constants
	private static final String GROUP_EXCHANGE_1_BUYER_1_LEAD_BUYER = "exchange_1_buyer_1_lead_buyer";
	private static final String GROUP_EXCHANGE_1_LEAD_SELLER = "exchange_1_lead_seller";
	private static final String GROUP_EXCHANGE_1_SELLER_SME = "exchange_1_seller_sme";
	
	//Meredith QA Process Variables - Exchange
	private static final String PROC_VAR_EXCHANGE_ID = "mrd_exchangeID";
	private static final String PROC_VAR_EXCHANGE_BUYER_TEAM = "mrd_exchangeBuyerTeam";
	private static final String PROC_VAR_EXCHANGE_LEAD_BUYER = "mrd_exchangeLeadBuyerGroup";
	private static final String PROC_VAR_EXCHANGE_LEAD_SELLER = "mrd_exchangeLeadSellerGroup";
	
	//Meredith QA Process Variables - Question
	private static final String PROC_VAR_QUESTION_OWNER = "mrd_questionOwner";
	private static final String PROC_VAR_QUESTION_STATUS = "mrd_questionStatus";
	private static final String PROC_VAR_QUESTION_TITLE = "mrd_questionTitle";
	private static final String PROC_VAR_QUESTION_CATEGORY = "mrd_questionCategory";
	private static final String PROC_VAR_QUESTION_BODY = "mrd_questionBody";
	private static final String PROC_VAR_QUESTION_COMMENT = "mrd_questionComment";
    private static final String PROC_VAR_QUESTION_AUTHOR_USER = "mrd_questionAuthorUser";
    private static final String PROC_VAR_QUESTION_AUTHOR_TEAM = "mrd_questionAuthorTeam";
    private static final String PROC_VAR_QUESTION_AUTHOR_TEAM_MER_ID = "mrd_questionAuthorTeamMerId";
    private static final String PROC_VAR_QUESTION_PRIORITY = "mrd_questionPriority";
    private static final String PROC_VAR_QUESTION_DOC_REF = "mrd_questionDocRef";

	//Meredith QA Process Variables - Answer
	private static final String PROC_VAR_ANSWER_STATUS = "mrd_answerStatus";
	private static final String PROC_VAR_ANSWER_BODY = "mrd_answerBody";
	private static final String PROC_VAR_ANSWER_COMMENT = "mrd_answerComment";
	
	//Meredith QA Process Variables - Subject Matter Expert
	private static final String PROC_VAR_SME = "mrd_subjectMatterExpertGroup";
	
	//Meredith QA - Question Status
	private static final Object VAR_QUESTION_STATUS_REJECTED = "rejected";
	private static final Object VAR_QUESTION_STATUS_REVISION = "revision";
	private static final Object VAR_QUESTION_STATUS_ACCEPTED = "accepted";

	//Meredith QA - Answer Status
	private static final Object VAR_ANSWER_STATUS_REVISION = "revision";
	private static final Object VAR_ANSWER_STATUS_ACCEPTED = "accepted";
	
	//Meredith QA - Process Variable Default Values - Exchange (Test Only)
	private static final String TEST_VAR_EXCHANGE_ID = "exchange_1";
	private static final String TEST_VAR_EXCHANGE_BUYER_TEAM = "buyer_team";
	private static final String TEST_VAR_EXCHANGE_LEAD_BUYER_USER = USER_EXCHANGE_1_BUYER_1_LEAD_BUYER_1;
	private static final String TEST_VAR_EXCHANGE_LEAD_BUYER_GROUP = GROUP_EXCHANGE_1_BUYER_1_LEAD_BUYER;
	private static final String TEST_VAR_EXCHANGE_LEAD_SELLER_USER = USER_EXCHANGE_1_LEAD_SELLER_1;
	private static final String TEST_VAR_EXCHANGE_LEAD_SELLER_GROUP = GROUP_EXCHANGE_1_LEAD_SELLER;
	
	//Meredith QA - Process Variable Default Values - Question (Test Only)
	private static final String TEST_VAR_QUESTION_OWNER = USER_EXCHANGE_1_BUYER_1_USER_1;
	private static final String TEST_VAR_QUESTION_TITLE = "question_title";
	private static final String TEST_VAR_QUESTION_CATEGORY = "question_category";
	private static final String TEST_VAR_QUESTION_BODY = "question_body";
	private static final String TEST_VAR_QUESTION_COMMENT = "question_comment";
	
	//Meredith QA - Process Variable Default Values - Answer (Test Only)
	private static final String TEST_VAR_ANSWER_BODY = "answer_body";
	private static final String TEST_VAR_ANSWER_COMMENT = "answer_comment";

	//Meredith QA - Process Variable Default Values - SME (Test Only)
	private static final String TEST_VAR_EXCHANGE_SELLER_SME_USER = USER_EXCHANGE_1_SELLER_SME_1;
	private static final String TEST_VAR_EXCHANGE_SELLER_SME_GROUP = GROUP_EXCHANGE_1_SELLER_SME;
	
	//Meredith QA Task Name
	private static final String TASK_NAME_AUTHORISE_QUESTION = "Authorise Question";
	private static final String TASK_NAME_REVISE_QUESTION = "Revise Question";
	private static final String TASK_NAME_ASSIGN_QUESTION = "Assign Question";
	private static final String TASK_NAME_ANSWER_QUESTION = "Answer Question";
	private static final String TASK_NAME_AUTHORISE_ANSWER = "Authorise Answer";

	//Activiti Services
	private RuntimeService theRuntimeService = null;
	private IdentityService theIdentityService = null;
	private TaskService theTaskService = null;
	
	/**
	 * Setup Users and Groups
	 */
	@Before
	public void setup(){
		
		//Instantiate the Services
		theRuntimeService = theActivitiRule.getRuntimeService();
		theIdentityService = theActivitiRule.getIdentityService();
		theTaskService = theActivitiRule.getTaskService();
		
        //Create the Users
        logger.debug("Creating Users");
        theIdentityService.saveUser(theIdentityService.newUser(USER_EXCHANGE_1_BUYER_1_USER_1));
        theIdentityService.saveUser(theIdentityService.newUser(USER_EXCHANGE_1_BUYER_1_USER_2));
        theIdentityService.saveUser(theIdentityService.newUser(USER_EXCHANGE_1_BUYER_1_LEAD_BUYER_1));
        theIdentityService.saveUser(theIdentityService.newUser(USER_EXCHANGE_1_BUYER_1_LEAD_BUYER_2));
        theIdentityService.saveUser(theIdentityService.newUser(USER_EXCHANGE_1_LEAD_SELLER_1));
        theIdentityService.saveUser(theIdentityService.newUser(USER_EXCHANGE_1_LEAD_SELLER_2));
        theIdentityService.saveUser(theIdentityService.newUser(USER_EXCHANGE_1_SELLER_SME_1));
        theIdentityService.saveUser(theIdentityService.newUser(USER_EXCHANGE_1_SELLER_SME_2));
        
        //Create the Groups
        logger.debug("Creating Groups");
        theIdentityService.saveGroup(theIdentityService.newGroup(GROUP_EXCHANGE_1_BUYER_1_LEAD_BUYER));
        theIdentityService.saveGroup(theIdentityService.newGroup(GROUP_EXCHANGE_1_LEAD_SELLER));
        theIdentityService.saveGroup(theIdentityService.newGroup(GROUP_EXCHANGE_1_SELLER_SME));
        
        //Initialise the Group Membership
        logger.debug("Creating Group Membership");
        theIdentityService.createMembership(USER_EXCHANGE_1_BUYER_1_LEAD_BUYER_1, GROUP_EXCHANGE_1_BUYER_1_LEAD_BUYER);
        theIdentityService.createMembership(USER_EXCHANGE_1_BUYER_1_LEAD_BUYER_2, GROUP_EXCHANGE_1_BUYER_1_LEAD_BUYER);
        theIdentityService.createMembership(USER_EXCHANGE_1_LEAD_SELLER_1, GROUP_EXCHANGE_1_LEAD_SELLER);
        theIdentityService.createMembership(USER_EXCHANGE_1_LEAD_SELLER_2, GROUP_EXCHANGE_1_LEAD_SELLER);
        theIdentityService.createMembership(USER_EXCHANGE_1_SELLER_SME_1, GROUP_EXCHANGE_1_SELLER_SME);
        theIdentityService.createMembership(USER_EXCHANGE_1_SELLER_SME_2, GROUP_EXCHANGE_1_SELLER_SME);
	}
	
	/**
	 * Remove Users and Groups
	 */
	@After
	public void tearDown(){
		
        //Delete the Groups
        logger.debug("Deleting Groups");
        theIdentityService.deleteGroup(GROUP_EXCHANGE_1_BUYER_1_LEAD_BUYER);
        theIdentityService.deleteGroup(GROUP_EXCHANGE_1_LEAD_SELLER);
        theIdentityService.deleteGroup(GROUP_EXCHANGE_1_SELLER_SME);
        
        //Delete the Users
        logger.debug("Deleting Users");
        theIdentityService.deleteUser(USER_EXCHANGE_1_BUYER_1_USER_1);
        theIdentityService.deleteUser(USER_EXCHANGE_1_BUYER_1_USER_2);
        theIdentityService.deleteUser(USER_EXCHANGE_1_BUYER_1_LEAD_BUYER_1);
        theIdentityService.deleteUser(USER_EXCHANGE_1_BUYER_1_LEAD_BUYER_2);
        theIdentityService.deleteUser(USER_EXCHANGE_1_LEAD_SELLER_1);
        theIdentityService.deleteUser(USER_EXCHANGE_1_LEAD_SELLER_2);
        theIdentityService.deleteUser(USER_EXCHANGE_1_SELLER_SME_1);
        theIdentityService.deleteUser(USER_EXCHANGE_1_SELLER_SME_2);
	}
	
	/**
	 * Starts the Process in Alfresco
	 * @return	ProcessInstance
	 * 				the Meredith QA Process Instance
	 */
	private ProcessInstance startQAProcess() {
		
		//Initialise the Process Variables
        final Map<String, Object> theVariables = new HashMap<String, Object>();
        
        //Exchange
        theVariables.put(PROC_VAR_EXCHANGE_ID, 				  TEST_VAR_EXCHANGE_ID);
        theVariables.put(PROC_VAR_EXCHANGE_BUYER_TEAM, 		  TEST_VAR_EXCHANGE_BUYER_TEAM);
        theVariables.put(PROC_VAR_EXCHANGE_LEAD_BUYER, 		  TEST_VAR_EXCHANGE_LEAD_BUYER_GROUP);
        theVariables.put(PROC_VAR_EXCHANGE_LEAD_SELLER, 	  TEST_VAR_EXCHANGE_LEAD_SELLER_GROUP);
        theVariables.put(PROC_VAR_QUESTION_AUTHOR_TEAM,       "Legal Admin");
        theVariables.put(PROC_VAR_QUESTION_AUTHOR_TEAM_MER_ID,"30001");
        theVariables.put(PROC_VAR_QUESTION_AUTHOR_USER,       "legal.admin@meredith.com");
        theVariables.put(PROC_VAR_QUESTION_DOC_REF,           "alfresco_dof_ref");
        theVariables.put(PROC_VAR_QUESTION_PRIORITY,          "H");


        //Question
        theVariables.put(PROC_VAR_QUESTION_OWNER, 			TEST_VAR_QUESTION_OWNER);
        theVariables.put(PROC_VAR_QUESTION_TITLE, 			TEST_VAR_QUESTION_TITLE);
        theVariables.put(PROC_VAR_QUESTION_CATEGORY, 		TEST_VAR_QUESTION_CATEGORY);
        theVariables.put(PROC_VAR_QUESTION_BODY, 			TEST_VAR_QUESTION_BODY);
                
        //Start the Process
        logger.debug("Starting the Meredith QA Process");
        final String theProcessInstanceID = theRuntimeService.startProcessInstanceByKey(PROCESS_ID, theVariables).getProcessInstanceId();
        final ProcessInstance theProcessInstance = theRuntimeService.createProcessInstanceQuery()
        														   .includeProcessVariables()
        														   .processInstanceId(theProcessInstanceID)
        														   .singleResult();
        //Output the Process Variables
        outputProcessVariables(theProcessInstance.getProcessVariables());
        
        //Return the Process Instance
        return theProcessInstance;
	}

	private void outputProcessVariables(final Map<String, Object> theProcessVariables) {
		
		//Output the Process Instance Variables
        final Set<Entry<String, Object>> theEntries = theProcessVariables.entrySet();
        for (Entry<String, Object> theVariable : theEntries){
        	logger.debug("Key: " + theVariable.getKey() + "\t\t\t" + "Value: " + theVariable.getValue().toString());
        }
	}
	
	/**
	 * Test - Process Start (No Errors)
	 */
    @Test
    @Deployment (resources={PROCESS_XML})
	public void testProcessStart() {

    	//Start the Process
        final ProcessInstance theProcessInstance = startQAProcess();
        assertNotNull("Process could not be started", theProcessInstance.getActivityId());
    }
    
	/**
	 * Test - Authorise Question - Reject
	 */
    @Test
    @Deployment (resources={PROCESS_XML})
	public void testAuthoriseQuestionReject() {

    	//Start the Process
        final ProcessInstance theProcessInstance = startQAProcess();
        assertNotNull("Process could not be started", theProcessInstance.getActivityId());
        
        //Obtain the Authorise Question Task
        logger.debug("Obtaining Authorise Question Task");
        Task theAuthoriseQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_BUYER_GROUP).singleResult();
        String theAuthoriseQuestionTaskName = theAuthoriseQuestionTask.getName();
        assertTrue("Task: " + theAuthoriseQuestionTaskName, theAuthoriseQuestionTaskName.equals(TASK_NAME_AUTHORISE_QUESTION));
        
        //Reject the Authorise Question Task
        logger.debug("Rejecting Authorise Question Task");
        theTaskService.claim(theAuthoriseQuestionTask.getId(), TEST_VAR_EXCHANGE_LEAD_BUYER_USER);
        Map<String, Object> theProcessVariables = theProcessInstance.getProcessVariables();
        theProcessVariables.put(PROC_VAR_QUESTION_STATUS, VAR_QUESTION_STATUS_REJECTED);
        theProcessVariables.put(PROC_VAR_QUESTION_COMMENT, TEST_VAR_QUESTION_COMMENT);
        theTaskService.complete(theAuthoriseQuestionTask.getId(), theProcessVariables);
        
        //Confirm Process Termination 
        logger.debug("Confirming Process Termination");
        ProcessInstance theTerminatedProcessInstance = theRuntimeService.createProcessInstanceQuery().processInstanceId(theProcessInstance.getId()).singleResult();
        assertNull(theTerminatedProcessInstance);
    }
        
	/**
	 * Test - Authorise Question - Revise
	 */
    @Test
    @Deployment (resources={PROCESS_XML})
	public void testAuthoriseQuestionRevise() {

    	//Start the Process
        final ProcessInstance theProcessInstance = startQAProcess();
        assertNotNull("Process could not be started", theProcessInstance.getActivityId());
        
        //Obtain the Authorise Question Task
        logger.debug("Obtaining Authorise Question Task");
        Task theAuthoriseQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_BUYER_GROUP).singleResult();
        String theAuthoriseQuestionTaskName = theAuthoriseQuestionTask.getName();
        assertTrue("Task: " + theAuthoriseQuestionTaskName, theAuthoriseQuestionTaskName.equals(TASK_NAME_AUTHORISE_QUESTION));
        
        //Revise the Authorise Question Task
        logger.debug("Rejecting Authorise Question Task");
        theTaskService.claim(theAuthoriseQuestionTask.getId(), TEST_VAR_EXCHANGE_LEAD_BUYER_USER);
        Map<String, Object> theProcessVariables = theProcessInstance.getProcessVariables();
        theProcessVariables.put(PROC_VAR_QUESTION_STATUS, VAR_QUESTION_STATUS_REVISION);
        theProcessVariables.put(PROC_VAR_QUESTION_COMMENT, TEST_VAR_QUESTION_COMMENT);
        theTaskService.complete(theAuthoriseQuestionTask.getId(), theProcessVariables);
        
        //Obtain the Revise Question Task
        logger.debug("Obtaining Revise Question Task");
        final Task theReviseQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateUser(TEST_VAR_QUESTION_OWNER).singleResult();
        final String theReviseQuestionTaskName = theReviseQuestionTask.getName();
        assertTrue("Task: " + theReviseQuestionTaskName, theReviseQuestionTaskName.equals(TASK_NAME_REVISE_QUESTION));
        
        //Complete the Revise Question Task
        logger.debug("Completing Revise Question Task");
        theTaskService.claim(theReviseQuestionTask.getId(), TEST_VAR_QUESTION_OWNER);
        theProcessVariables = theProcessInstance.getProcessVariables();
        theTaskService.complete(theReviseQuestionTask.getId(), theProcessVariables);
        
        //Obtain the Authorise Question Task
        logger.debug("Obtaining Authorise Question Task");
        theAuthoriseQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_BUYER_GROUP).singleResult();
        theAuthoriseQuestionTaskName = theAuthoriseQuestionTask.getName();
        assertTrue("Task: " + theAuthoriseQuestionTaskName, theAuthoriseQuestionTaskName.equals(TASK_NAME_AUTHORISE_QUESTION));
    }
    
	/**
	 * Test - Authorise Question - Accept
	 */
    @Test
    @Deployment (resources={PROCESS_XML})
	public void testAuthoriseQuestionAccept() {

    	//Start the Process
        final ProcessInstance theProcessInstance = startQAProcess();
        assertNotNull("Process could not be started", theProcessInstance.getActivityId());
        
        //Obtain the Authorise Question Task
        logger.debug("Obtaining Authorise Question Task");
        final Task theAuthoriseQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_BUYER_GROUP).singleResult();
        final String theAuthoriseQuestionTaskName = theAuthoriseQuestionTask.getName();
        assertTrue("Task: " + theAuthoriseQuestionTaskName, theAuthoriseQuestionTaskName.equals(TASK_NAME_AUTHORISE_QUESTION));
        
        //Accept the Authorise Question Task
        logger.debug("Accepting Authorise Question Task");
        theTaskService.claim(theAuthoriseQuestionTask.getId(), TEST_VAR_EXCHANGE_LEAD_BUYER_USER);
        Map<String, Object> theProcessVariables = theProcessInstance.getProcessVariables();
        theProcessVariables.put(PROC_VAR_QUESTION_STATUS, VAR_QUESTION_STATUS_ACCEPTED);
        theProcessVariables.put(PROC_VAR_QUESTION_COMMENT, TEST_VAR_QUESTION_COMMENT);
        theTaskService.complete(theAuthoriseQuestionTask.getId(), theProcessVariables);
        
        //Obtain the Assign Question Task
        logger.debug("Obtaining Assign Question Task");
        final Task theAssignQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_SELLER_GROUP).singleResult();
        final String theAssignQuestionTaskName = theAssignQuestionTask.getName();
        assertTrue("Task: " + theAssignQuestionTaskName, theAssignQuestionTaskName.equals(TASK_NAME_ASSIGN_QUESTION));
    }
    
	/**
	 * Test - Assign Question
	 */
    @Test
    @Deployment (resources={PROCESS_XML})
	public void testAssignQuestion() {

    	//Start the Process
        final ProcessInstance theProcessInstance = startQAProcess();
        assertNotNull("Process could not be started", theProcessInstance.getActivityId());
        
        //Obtain the Authorise Question Task
        logger.debug("Obtaining Authorise Question Task");
        final Task theAuthoriseQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_BUYER_GROUP).singleResult();
        final String theAuthoriseQuestionTaskName = theAuthoriseQuestionTask.getName();
        assertTrue("Task: " + theAuthoriseQuestionTaskName, theAuthoriseQuestionTaskName.equals(TASK_NAME_AUTHORISE_QUESTION));
        
        //Accept the Authorise Question Task
        logger.debug("Accepting Authorise Question Task");
        theTaskService.claim(theAuthoriseQuestionTask.getId(), TEST_VAR_EXCHANGE_LEAD_BUYER_USER);
        Map<String, Object> theProcessVariables = theProcessInstance.getProcessVariables();
        theProcessVariables.put(PROC_VAR_QUESTION_STATUS, VAR_QUESTION_STATUS_ACCEPTED);
        theTaskService.complete(theAuthoriseQuestionTask.getId(), theProcessVariables);
        
        //Obtain the Assign Question Task
        logger.debug("Obtaining Assign Question Task");
        final Task theAssignQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_SELLER_GROUP).singleResult();
        final String theAssignQuestionTaskName = theAssignQuestionTask.getName();
        assertTrue("Task: " + theAssignQuestionTaskName, theAssignQuestionTaskName.equals(TASK_NAME_ASSIGN_QUESTION));

        //Accept the Assign Question Task
        logger.debug("Accepting Assign Question Task");
        theTaskService.claim(theAssignQuestionTask.getId(), TEST_VAR_EXCHANGE_LEAD_SELLER_USER);
        theProcessVariables = theProcessInstance.getProcessVariables();
        theProcessVariables.put(PROC_VAR_SME, TEST_VAR_EXCHANGE_SELLER_SME_GROUP);
        theTaskService.complete(theAssignQuestionTask.getId(), theProcessVariables);
        
        //Obtain the Answer Question Task
        logger.debug("Obtaining Answer Question Task");
        final Task theAnswerQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_SELLER_SME_GROUP).singleResult();
        final String theAnswerQuestionTaskName = theAnswerQuestionTask.getName();
        assertTrue("Task: " + theAnswerQuestionTaskName, theAnswerQuestionTaskName.equals(TASK_NAME_ANSWER_QUESTION));
    }

	/**
	 * Test - Answer Question
	 */
    @Test
    @Deployment (resources={PROCESS_XML})
	public void testAnswerQuestion() {

    	//Start the Process
        final ProcessInstance theProcessInstance = startQAProcess();
        assertNotNull("Process could not be started", theProcessInstance.getActivityId());
        
        //Obtain the Authorise Question Task
        logger.debug("Obtaining Authorise Question Task");
        final Task theAuthoriseQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_BUYER_GROUP).singleResult();
        final String theAuthoriseQuestionTaskName = theAuthoriseQuestionTask.getName();
        assertTrue("Task: " + theAuthoriseQuestionTaskName, theAuthoriseQuestionTaskName.equals(TASK_NAME_AUTHORISE_QUESTION));
        
        //Accept the Authorise Question Task
        logger.debug("Accepting Authorise Question Task");
        theTaskService.claim(theAuthoriseQuestionTask.getId(), TEST_VAR_EXCHANGE_LEAD_BUYER_USER);
        Map<String, Object> theProcessVariables = theProcessInstance.getProcessVariables();
        theProcessVariables.put(PROC_VAR_QUESTION_STATUS, VAR_QUESTION_STATUS_ACCEPTED);
        theTaskService.complete(theAuthoriseQuestionTask.getId(), theProcessVariables);
        
        //Obtain the Assign Question Task
        logger.debug("Obtaining Assign Question Task");
        final Task theAssignQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_SELLER_GROUP).singleResult();
        final String theAssignQuestionTaskName = theAssignQuestionTask.getName();
        assertTrue("Task: " + theAssignQuestionTaskName, theAssignQuestionTaskName.equals(TASK_NAME_ASSIGN_QUESTION));
        
        //Accept the Assign Question Task
        logger.debug("Accepting Assign Question Task");
        theTaskService.claim(theAssignQuestionTask.getId(), TEST_VAR_EXCHANGE_LEAD_SELLER_USER);
        theProcessVariables = theProcessInstance.getProcessVariables();
        theProcessVariables.put(PROC_VAR_SME, TEST_VAR_EXCHANGE_SELLER_SME_GROUP);
        theTaskService.complete(theAssignQuestionTask.getId(), theProcessVariables);
        
        //Obtain the Answer Question Task
        logger.debug("Obtaining Answer Question Task");
        final Task theAnswerQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_SELLER_SME_GROUP).singleResult();
        final String theAnswerQuestionTaskName = theAnswerQuestionTask.getName();
        assertTrue("Task: " + theAnswerQuestionTaskName, theAnswerQuestionTaskName.equals(TASK_NAME_ANSWER_QUESTION));
        
        //Accept the Answer Question Task
        logger.debug("Accepting Answer Question Task");
        theTaskService.claim(theAnswerQuestionTask.getId(), TEST_VAR_EXCHANGE_SELLER_SME_USER);
        theProcessVariables = theProcessInstance.getProcessVariables();
        theProcessVariables.put(PROC_VAR_ANSWER_BODY, TEST_VAR_ANSWER_BODY);
        theTaskService.complete(theAnswerQuestionTask.getId(), theProcessVariables);
        
        //Obtain the Approve Answer Task
        logger.debug("Obtaining Approve Answer Task");
        final Task theApproveAnswerTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_SELLER_GROUP).singleResult();
        final String theApproveAnswerTaskName = theApproveAnswerTask.getName();
        assertTrue("Task: " + theApproveAnswerTaskName, theApproveAnswerTaskName.equals(TASK_NAME_AUTHORISE_ANSWER));
    }
    
    /**
	 * Test - Approve Answer - Rejected
	 */
    @Test
    @Deployment (resources={PROCESS_XML})
  	public void testApproveAnswerRejected() {

    	//Start the Process
    	final ProcessInstance theProcessInstance = startQAProcess();
    	assertNotNull("Process could not be started", theProcessInstance.getActivityId());
	  
    	//Obtain the Authorise Question Task
    	logger.debug("Obtaining Authorise Question Task");
    	final Task theAuthoriseQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_BUYER_GROUP).singleResult();
    	final String theAuthoriseQuestionTaskName = theAuthoriseQuestionTask.getName();
    	assertTrue("Task: " + theAuthoriseQuestionTaskName, theAuthoriseQuestionTaskName.equals(TASK_NAME_AUTHORISE_QUESTION));
	  
    	//Accept the Authorise Question Task
    	logger.debug("Accepting Authorise Question Task");
    	theTaskService.claim(theAuthoriseQuestionTask.getId(), TEST_VAR_EXCHANGE_LEAD_BUYER_USER);
    	Map<String, Object> theProcessVariables = theProcessInstance.getProcessVariables();
    	theProcessVariables.put(PROC_VAR_QUESTION_STATUS, VAR_QUESTION_STATUS_ACCEPTED);
    	theTaskService.complete(theAuthoriseQuestionTask.getId(), theProcessVariables);
	  
    	//Obtain the Assign Question Task
    	logger.debug("Obtaining Assign Question Task");
    	final Task theAssignQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_SELLER_GROUP).singleResult();
    	final String theAssignQuestionTaskName = theAssignQuestionTask.getName();
    	assertTrue("Task: " + theAssignQuestionTaskName, theAssignQuestionTaskName.equals(TASK_NAME_ASSIGN_QUESTION));
	  
        //Accept the Assign Question Task
        logger.debug("Accepting Assign Question Task");
        theTaskService.claim(theAssignQuestionTask.getId(), TEST_VAR_EXCHANGE_LEAD_SELLER_USER);
        theProcessVariables = theProcessInstance.getProcessVariables();
        theProcessVariables.put(PROC_VAR_SME, TEST_VAR_EXCHANGE_SELLER_SME_GROUP);
        theTaskService.complete(theAssignQuestionTask.getId(), theProcessVariables);
        
        //Obtain the Answer Question Task
        logger.debug("Obtaining Answer Question Task");
        Task theAnswerQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_SELLER_SME_GROUP).singleResult();
        String theAnswerQuestionTaskName = theAnswerQuestionTask.getName();
        assertTrue("Task: " + theAnswerQuestionTaskName, theAnswerQuestionTaskName.equals(TASK_NAME_ANSWER_QUESTION));
	  
    	//Accept the Answer Question Task
	 	logger.debug("Accepting Answer Question Task");
	 	theTaskService.claim(theAnswerQuestionTask.getId(), TEST_VAR_EXCHANGE_SELLER_SME_USER);
	 	theProcessVariables = theProcessInstance.getProcessVariables();
	 	theProcessVariables.put(PROC_VAR_ANSWER_BODY, TEST_VAR_ANSWER_BODY);
	 	theTaskService.complete(theAnswerQuestionTask.getId(), theProcessVariables);
	  
	 	//Obtain the Approve Answer Task
	 	logger.debug("Obtaining Approve Answer Task");
	 	final Task theApproveAnswerTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_SELLER_GROUP).singleResult();
	 	final String theApproveAnswerTaskName = theApproveAnswerTask.getName();
	 	assertTrue("Task: " + theApproveAnswerTaskName, theApproveAnswerTaskName.equals(TASK_NAME_AUTHORISE_ANSWER));
	  
	 	//Accept the Approve Answer Task
	 	logger.debug("Accepting Approve Answer Task");
	 	theTaskService.claim(theApproveAnswerTask.getId(), TEST_VAR_EXCHANGE_LEAD_SELLER_USER);
	 	theProcessVariables = theProcessInstance.getProcessVariables();
	 	theProcessVariables.put(PROC_VAR_ANSWER_STATUS, VAR_ANSWER_STATUS_REVISION);
	 	theProcessVariables.put(PROC_VAR_ANSWER_COMMENT, TEST_VAR_ANSWER_COMMENT);
	 	theTaskService.complete(theApproveAnswerTask.getId(), theProcessVariables);
	  
        //Obtain the Answer Question Task
        logger.debug("Obtaining Answer Question Task");
        theAnswerQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_SELLER_SME_GROUP).singleResult();
        theAnswerQuestionTaskName = theAnswerQuestionTask.getName();
        assertTrue("Task: " + theAnswerQuestionTaskName, theAnswerQuestionTaskName.equals(TASK_NAME_ANSWER_QUESTION));
    }
    
    /**
	 * Test - Approve Answer - Approved
	 */
    @Test
    @Deployment (resources={PROCESS_XML})
  	public void testApproveAnswerAccepted() {

    	//Start the Process
    	final ProcessInstance theProcessInstance = startQAProcess();
    	assertNotNull("Process could not be started", theProcessInstance.getActivityId());
	  
    	//Obtain the Authorise Question Task
    	logger.debug("Obtaining Authorise Question Task");
    	final Task theAuthoriseQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_BUYER_GROUP).singleResult();
    	final String theAuthoriseQuestionTaskName = theAuthoriseQuestionTask.getName();
    	assertTrue("Task: " + theAuthoriseQuestionTaskName, theAuthoriseQuestionTaskName.equals(TASK_NAME_AUTHORISE_QUESTION));
	  
    	//Accept the Authorise Question Task
    	logger.debug("Accepting Authorise Question Task");
    	theTaskService.claim(theAuthoriseQuestionTask.getId(), TEST_VAR_EXCHANGE_LEAD_BUYER_USER);
    	Map<String, Object> theProcessVariables = theProcessInstance.getProcessVariables();
    	theProcessVariables.put(PROC_VAR_QUESTION_STATUS, VAR_QUESTION_STATUS_ACCEPTED);
    	theTaskService.complete(theAuthoriseQuestionTask.getId(), theProcessVariables);
	  
    	//Obtain the Assign Question Task
    	logger.debug("Obtaining Assign Question Task");
    	final Task theAssignQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_SELLER_GROUP).singleResult();
    	final String theAssignQuestionTaskName = theAssignQuestionTask.getName();
    	assertTrue("Task: " + theAssignQuestionTaskName, theAssignQuestionTaskName.equals(TASK_NAME_ASSIGN_QUESTION));
	  
        //Accept the Assign Question Task
        logger.debug("Accepting Assign Question Task");
        theTaskService.claim(theAssignQuestionTask.getId(), TEST_VAR_EXCHANGE_LEAD_SELLER_USER);
        theProcessVariables = theProcessInstance.getProcessVariables();
        theProcessVariables.put(PROC_VAR_SME, TEST_VAR_EXCHANGE_SELLER_SME_GROUP);
        theTaskService.complete(theAssignQuestionTask.getId(), theProcessVariables);
        
        //Obtain the Answer Question Task
        logger.debug("Obtaining Answer Question Task");
        Task theAnswerQuestionTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_SELLER_SME_GROUP).singleResult();
        String theAnswerQuestionTaskName = theAnswerQuestionTask.getName();
        assertTrue("Task: " + theAnswerQuestionTaskName, theAnswerQuestionTaskName.equals(TASK_NAME_ANSWER_QUESTION));
	  
    	//Accept the Answer Question Task
	 	logger.debug("Accepting Answer Question Task");
	 	theTaskService.claim(theAnswerQuestionTask.getId(), TEST_VAR_EXCHANGE_SELLER_SME_USER);
	 	theProcessVariables = theProcessInstance.getProcessVariables();
	 	theProcessVariables.put(PROC_VAR_ANSWER_BODY, TEST_VAR_ANSWER_BODY);
	 	theTaskService.complete(theAnswerQuestionTask.getId(), theProcessVariables);
	  
	 	//Obtain the Approve Answer Task
	 	logger.debug("Obtaining Approve Answer Task");
	 	final Task theApproveAnswerTask = (Task) theTaskService.createTaskQuery().active().taskCandidateGroup(TEST_VAR_EXCHANGE_LEAD_SELLER_GROUP).singleResult();
	 	final String theApproveAnswerTaskName = theApproveAnswerTask.getName();
	 	assertTrue("Task: " + theApproveAnswerTaskName, theApproveAnswerTaskName.equals(TASK_NAME_AUTHORISE_ANSWER));
	  
	 	//Accept the Approve Answer Task
	 	logger.debug("Accepting Approve Answer Task");
	 	theTaskService.claim(theApproveAnswerTask.getId(), TEST_VAR_EXCHANGE_LEAD_SELLER_USER);
	 	theProcessVariables = theProcessInstance.getProcessVariables();
	 	theProcessVariables.put(PROC_VAR_ANSWER_STATUS, VAR_ANSWER_STATUS_ACCEPTED);
	 	theTaskService.complete(theApproveAnswerTask.getId(), theProcessVariables);
    }
}
